export const sortPosts = (posts, sortCriterion) => {
    switch (sortCriterion) {
      case 'newest':
        return [...posts].sort((a, b) => new Date(b.createdOn.dateTime) - new Date(a.createdOn.dateTime));
      case 'oldest':
        return [...posts].sort((a, b) => new Date(a.createdOn.dateTime) - new Date(b.createdOn.dateTime));
      case 'likes':
        return [...posts].sort((a, b) => b.likeCount - a.likeCount);
      case 'comments':
        return [...posts].sort((a, b) => b.commentCount - a.commentCount);
      default:
        return posts;
    }
  };
  

  export const sortOptions = [
    { value: 'newest', label: 'Newest' },
    { value: 'oldest', label: 'Oldest' },
    { value: 'likes', label: 'Most Liked' },
    { value: 'comments', label: 'Most Commented' },
  ];

  export const sortUserOptions = [
    { value: 'newest', label: 'Newest' },
    { value: 'oldest', label: 'Oldest' },
  ];

  export const commentSortOptions = [
    { value: 'newest', label: 'Newest' },
    { value: 'oldest', label: 'Oldest' },
  ]