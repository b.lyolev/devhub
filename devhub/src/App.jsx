import { BrowserRouter } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Home from './Views/Home/Home';
import AuthContext from './context/AuthContext';
import { useEffect, useState } from 'react';
import { getUserData } from './services/user.service';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase-config';
import Login from './Views/Login/Login';
import SignUp from './Views/SignUp/SignUp';
import AdminDashboard from './Views/AdminDashboard/AdminDashboard';
import ProfileCard from './components/ProfileCard/ProfileCard';
import PostPreview from './Views/Post Preview/PostPreview';
import Settings from './Views/Settings/Settings';
import LoginModal from './Views/Login/LoginModal';
import YourProfile from './Views/Your Profile/YourProfile';

function App() {
  const [user, loading] = useAuthState(auth);
  const [isInitialized, setIsInitialized] = useState(false);

  const setOpenLoginModal = () => {};

  const [appState, setAppState] = useState({
    user,
    userData: null,
  });

  if (appState.user !== user) {
    setAppState({ user });
  }

  useEffect(() => {
    if (loading) return;
    setIsInitialized(true);
  }, [loading]);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setAppState({
          ...appState,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => alert(e.message));
  }, [user]);

  return (
    <>
      <div className='bg-[url(https://snippets.alexandru.so/grid.svg)] h-full'>
        <BrowserRouter>
          {isInitialized && (
            <AuthContext.Provider value={{ ...appState, setUser: setAppState }}>
              <Routes>
                <Route path='/' element={<Home />} />
                <Route
                  path='/post/:postId'
                  element={
                    user ? (
                      <PostPreview />
                    ) : (
                      <LoginModal open={true} setOpen={setOpenLoginModal} />
                    )
                  }
                />
                {/* {console.log(appState.userData)} */}
                <Route path='/login' element={<Login />} />
                <Route path='/signup' element={<SignUp />} />

                <Route path='/admin-dashboard' element={<AdminDashboard />} />
                <Route path='/profile-card' element={<ProfileCard />} />
                <Route path='/settings' element={<Settings />} />

                <Route
                  path='/profile/'
                  element={
                    user ? (
                      <YourProfile />
                    ) : (
                      <LoginModal open={true} setOpen={setOpenLoginModal} />
                    )
                  }
                />
              </Routes>
            </AuthContext.Provider>
          )}

        </BrowserRouter>
      </div>
    </>
  );
}

export default App;
