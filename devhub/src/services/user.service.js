import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
} from 'firebase/database';
import { auth, db, storage } from '../config/firebase-config';
import {
  deleteObject,
  getDownloadURL,
  uploadBytes,
  ref as uRef,
} from 'firebase/storage';
import { updateEmail, updatePassword, updateProfile } from 'firebase/auth';
import Alert from '../components/Alert/Alert';

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (handle, uid, firstName, lastName, email) => {
  const now = new Date();
  const createdOnTimestamp = now.getTime(); // Unix timestamp in milliseconds
  const createdOnDateTime = now.toISOString(); // ISO string representation of date

  return set(ref(db, `users/${handle}`), {
    handle,
    uid,
    firstName,
    lastName,
    email,
    createdOn: {
      timestamp: createdOnTimestamp,
      dateTime: createdOnDateTime,
    },
    profileImage: null,
  });
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateUserProfile = async (handle, updatedData) => {
  const { email, password, ...otherUpdatedData } = updatedData;

  try {
    const userRef = ref(db, `users/${handle}`);
    // If the email is being updated, use the Firebase Auth API to update it
    if (email) {
      await updateEmail(auth.currentUser, email);
      await update(userRef, { email });
    }

    // If the password is being updated, use the Firebase Auth API to update it
    if (password) {
      await updatePassword(auth.currentUser, password);
    }

    // Construct the reference to the user's data in the database

    // Update the user's profile data excluding email and password using the database update function
    await update(userRef, otherUpdatedData);

    // If the email or password were updated successfully, update the user's profile object
    if (email || password) {
      await updateProfile(auth.currentUser, { email });
    }

    return true; // Indicate successful update
  } catch (error) {
    Alert('Error updating user profile:', error);
    return false; // Indicate failed update
  }
};

export const getAllUsersPosts = async () => {
  const usersSnapshot = await get(ref(db, 'users'));
  const usersData = usersSnapshot.val();
  let aggregatedData = [];

  for (let handle in usersData) {
    const userPostsSnapshot = await get(ref(db, `posts/${handle}`));
    aggregatedData.push({
      handle: handle,
      postsCount: Object.keys(userPostsSnapshot.val() || {}).length,
      ...usersData[handle],
    });
  }
  return aggregatedData;
};

export const updateProfileImage = (handle, imageUrl) => {
  return update(ref(db, `users/${handle}`), { profileImage: imageUrl });
};
export const uploadImageAndSaveLink = async (handle, selectedFile) => {
  const newStorageRef = uRef(
    storage,
    `profileImages/${handle}/${selectedFile.name}`
  );

  await uploadBytes(newStorageRef, selectedFile);
  const imageUrl = await getDownloadURL(newStorageRef);
  const userSnapshot = await getUserByHandle(handle);
  const userData = userSnapshot.val();

  if (userData.profileImage) {
    const prevImageRef = uRef(storage, userData.profileImage);
    await deleteObject(prevImageRef);
  }

  return updateProfileImage(handle, imageUrl);
};
