import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  onValue,
  update,
  increment,
  remove,
} from 'firebase/database';
import { db } from '../config/firebase-config';
import { v4 as uuidv4 } from 'uuid';

export const getPostByUID = (uid) => {
  return get(ref(db, `posts/${uid}`));
};

/**
 * @param {string} authorHandle - The handle of the post author.
 * @param {string} uid - The UID of the post.
 * @param {string} authorUID - The UID of the post author.
 * @param {string} author - The name or identifier of the post author.
 * @param {string} title - The title of the post.
 * @param {string} content - The content or body of the post.
 * @param {string} category - The category of the post.
 * @param {string[]} tags - The tags of the post.
 * @returns {Promise<void>} A Promise that resolves when the post creation is successful and rejects in case of any errors.
 * @description Creates a new post in the database.
 */
export const createPostHandle = async (
  authorHandle,
  authorUID,
  author,
  title,
  content,
  category,
  tags
) => {
  const uid = uuidv4();
  const now = new Date();
  const createdOnTimestamp = now.getTime();
  const createdOnDateTime = now.toISOString();

  const postRef = ref(db, `posts/${uid}`);
  const postData = {
    uid,
    authorHandle,
    authorUID,
    author,
    title,
    content,
    category,
    tags,
    views: 0,
    createdOn: {
      timestamp: createdOnTimestamp,
      dateTime: createdOnDateTime,
    },
    comments: {},
    likedBy: {},
  };

  try {
    await set(postRef, postData);

    // Update the user's createdPosts object
    const userCreatedPostsRef = ref(db, `users/${authorHandle}/createdPosts`);
    await update(userCreatedPostsRef, {
      [uid]: true, // Set the post UID as a key with value true
    });

    return uid; // Return the UID of the created post
  } catch (error) {
    console.error('Error creating post:', error);
    throw error;
  }
};

export const getCreatedPostsForUser = (userUID, callback) => {
  const userCreatedPostsRef = ref(db, `users/${userUID}/createdPosts`);

  onValue(userCreatedPostsRef, (snapshot) => {
    const createdPosts = [];
    snapshot.forEach((childSnapshot) => {
      if (childSnapshot.val() === true) {
        createdPosts.push(childSnapshot.key);
      }
    });

    callback(createdPosts);
  });
};

export const incrementViewCount = (uid) => {
  const postRef = ref(db, `posts/${uid}`);
  return update(postRef, {
    views: increment(1),
  });
};

export const getSinglePostByUID = (uid) => {
  return get(ref(db, `posts/${uid}`));
};
export const getPostData = (uid) => {
  return getSinglePostByUID(uid);
};

export const getPostsByCategory = (category, callback) => {
  const postsRef = ref(db, 'posts');

  let categoryQuery;

  if (category) {
    categoryQuery = query(
      postsRef,
      orderByChild('category'),
      equalTo(category)
    );
  } else {
    categoryQuery = postsRef;
  }

  onValue(categoryQuery, (snapshot) => {
    const posts = [];
    snapshot.forEach((childSnapshot) => {
      posts.push(childSnapshot.val());
    });

    callback(posts);
  });
};

export const updatePostTitle = (uid, newTitle) => {
  return set(ref(db, `posts/${uid}/title`), newTitle);
};

export const updatePostContent = async (postUID, newContent) => {
  try {
    const postRef = ref(db, `posts/${postUID}`);
    const postSnapshot = await get(postRef);

    if (postSnapshot.exists()) {
      const postData = postSnapshot.val();
      postData.content = newContent;

      await set(postRef, postData);

      return true; // Indicate successful update
    } else {
      return false; // Indicate post doesn't exist
    }
  } catch (error) {
    console.error('Error updating post content:', error);
    throw error;
  }
};

export const createComment = (authorUID, content, postUID, authorHandle) => {
  const now = new Date();
  const createdOnTimestamp = now.getTime();
  const createdOnDateTime = now.toISOString();

  const commentUID = uuidv4();

  const commentData = {
    uid: commentUID,
    authorUID,
    authorHandle,
    postUID,
    content,
    createdOn: {
      timestamp: createdOnTimestamp,
      dateTime: createdOnDateTime,
    },
  };

  const commentRef = ref(db, `comments/${commentUID}`);

  return set(commentRef, commentData)
    .then(() => {
      const userCommentsRef = ref(
        db,
        `users/${authorHandle}/comments/${commentUID}`
      );
      return set(userCommentsRef, true);
    })
    .then(() => {
      const postCommentsRef = ref(
        db,
        `posts/${postUID}/comments/${commentUID}`
      );
      return set(postCommentsRef, true);
    })
    .then(() => {
      const userCommentsKeyRef = ref(db, `users/${authorHandle}/comments`);
      return update(userCommentsKeyRef, {
        [commentUID]: true,
      });
    })
    .catch((error) => {
      console.error('Error creating comment:', error);
      throw error;
    });
};

export const getCommentsForPost = (postId, callback) => {
  const commentsRef = ref(db, `posts/${postId}/comments`);

  onValue(commentsRef, (snapshot) => {
    const commentKeys = [];
    if (snapshot.exists()) {
      snapshot.forEach((childSnapshot) => {
        commentKeys.push(childSnapshot.key);
      });

      const commentsDataPromises = commentKeys.map((commentKey) => {
        const commentDataRef = ref(db, `comments/${commentKey}`);
        return get(commentDataRef).then((commentSnapshot) => {
          return commentSnapshot.val();
        });
      });

      Promise.all(commentsDataPromises).then((commentsData) => {
        const filteredCommentsData = commentsData.filter(
          (comment) => comment !== null
        );
        callback(filteredCommentsData);
      });
    } else {
      callback([]);
    }
  });
};

export const deleteCommentByUID = (
  commentUID,
  authorUID,
  postUID,
  authorHandle
) => {
  const commentRef = ref(db, `comments/${commentUID}`);
  const userCommentsRef = ref(
    db,
    `users/${authorHandle}/comments/${commentUID}`
  );
  const postCommentsRef = ref(db, `posts/${postUID}/comments/${commentUID}`);
  const userCommentsKeyRef = ref(db, `users/${authorHandle}/comments`);

  return Promise.all([
    remove(commentRef),
    remove(userCommentsRef),
    remove(postCommentsRef),
    update(userCommentsKeyRef, {
      [commentUID]: null,
    }),
  ])
    .then(() => {
      console.log('Comment deleted successfully.');
    })
    .catch((error) => {
      console.error('Error deleting comment:', error);
      throw error;
    });
};

export const updatePostLikedBy = async (postUID, userUID) => {
  try {
    const postLikedByRef = ref(db, `posts/${postUID}/likedBy/${userUID}`);
    return set(postLikedByRef, true);
  } catch (error) {
    console.error('Error updating post likedBy:', error);
    throw error;
  }
};

export const updateLikedPostsArray = async (userUID, postUID) => {
  try {
    const userLikedPostsRef = ref(db, `users/${userUID}/likedPosts/${postUID}`);
    return set(userLikedPostsRef, true);
  } catch (error) {
    console.error('Error updating user likedPosts:', error);
    throw error;
  }
};

export const togglePostLike = async (
  userUID,
  postUID,
  currentUserHandle,
  liked
) => {
  try {
    if (!liked) {
      await updateLikedPostsArray(currentUserHandle, postUID);
      await updatePostLikedBy(postUID, userUID);
      return true; // Indicate that the post has been liked
    } else {
      const userLikedPostsRef = ref(
        db,
        `users/${currentUserHandle}/likedPosts/${postUID}`
      );
      const postLikedByRef = ref(db, `posts/${postUID}/likedBy/${userUID}`);

      await Promise.all([remove(userLikedPostsRef), remove(postLikedByRef)]);
      return false; // Indicate that the like has been removed
    }
  } catch (error) {
    console.error('Error handling like:', error);
    throw error;
  }
};

export const updateCommentContent = (commentUID, newContent) => {
  const now = new Date();
  const editedTimestamp = now.getTime();
  const editedDateTime = now.toISOString();

  const commentRef = ref(db, `comments/${commentUID}`);
  return update(commentRef, {
    content: newContent,
    edited: {
      timestamp: editedTimestamp,
      dateTime: editedDateTime,
    },
  });
};

export const deletePostAndRelatedData = async (postUID) => {
  try {
    const postRef = ref(db, `posts/${postUID}`);
    const postSnapshot = await get(postRef);

    if (postSnapshot.exists()) {
      const post = postSnapshot.val();

      // Delete comments associated with the post
      if (post.comments) {
        for (const commentUID of Object.keys(post.comments)) {
          await deleteCommentByUID(
            commentUID,
            post.authorUID,
            postUID,
            post.authorHandle
          );
        }
      }

      // Remove the post from the user's createdPosts
      if (post.authorUID && post.authorHandle) {
        const userCreatedPostsRef = ref(
          db,
          `users/${post.authorHandle}/createdPosts/${postUID}`
        );
        await remove(userCreatedPostsRef);
      }

      // Remove the post's likedBy data
      if (post.likedBy) {
        for (const userUID of Object.keys(post.likedBy)) {
          const userLikedPostsRef = ref(
            db,
            `users/${userUID}/likedPosts/${postUID}`
          );
          await remove(userLikedPostsRef);
        }
      }

      // Delete the post itself
      await remove(postRef);

      return true; // Indicate successful deletion
    } else {
      return false; // Indicate post doesn't exist
    }
  } catch (error) {
    console.error('Error deleting post:', error);
    throw error;
  }
};

export const getAllPosts = async () => {
  const postsRef = ref(db, 'posts');

  return new Promise((resolve, reject) => {
    onValue(
      postsRef,
      (snapshot) => {
        const posts = [];
        snapshot.forEach((childSnapshot) => {
          posts.push(childSnapshot.val());
        });

        resolve(posts);
      },
      (error) => {
        reject(error);
      }
    );
  });
};

export const getAllUsers = async () => {
  const postsRef = ref(db, 'users');

  return new Promise((resolve, reject) => {
    onValue(
      postsRef,
      (snapshot) => {
        const posts = [];
        snapshot.forEach((childSnapshot) => {
          posts.push(childSnapshot.val());
        });

        resolve(posts);
      },
      (error) => {
        reject(error);
      }
    );
  });
};

export const getAllPostsByUserHandle = async (userHandle) => {
  const userRef = ref(db, `users/${userHandle}`);
  const userSnapshot = await get(userRef);

  if (userSnapshot.exists()) {
    const user = userSnapshot.val();

    if (user.createdPosts) {
      const postKeys = Object.keys(user.createdPosts);
      const postsDataPromises = postKeys.map((postKey) => {
        const postRef = ref(db, `posts/${postKey}`);
        return get(postRef).then((postSnapshot) => {
          return postSnapshot.val();
        });
      });

      return Promise.all(postsDataPromises);
    } else {
      return [];
    }
  } else {
    throw new Error('User not found');
  }
};

export const getAllCommentsByUserHandle = async (userHandle) => {
  try {
    const userRef = ref(db, `users/${userHandle}`);
    const userSnapshot = await get(userRef);

    if (userSnapshot.exists()) {
      const user = userSnapshot.val();

      if (user.comments) {
        const commentKeys = Object.keys(user.comments);
        const commentsDataPromises = commentKeys.map((commentKey) => {
          const commentRef = ref(db, `comments/${commentKey}`);
          return get(commentRef).then((commentSnapshot) => {
            return commentSnapshot.val();
          });
        });

        return Promise.all(commentsDataPromises);
      } else {
        return [];
      }
    } else {
      throw new Error('User not found');
    }
  } catch (error) {
    console.error('Error fetching comments:', error);
    throw error;
  }
};

export const blockUser = async (userHandle) => {
  try {
    const userRef = ref(db, `users/${userHandle}`);
    await update(userRef, {
      isBlocked: true,
    });
    return true;
  } catch (error) {
    console.error('Error blocking user:', error);
    throw error;
  }
};

export const unblockUser = async (userHandle) => {
  try {
    const userRef = ref(db, `users/${userHandle}`);
    await update(userRef, {
      isBlocked: false,
    });
    return true;
  } catch (error) {
    console.error('Error unblocking user:', error);
    throw error;
  }
};

export const getAllComments = async () => {
  const commentsRef = ref(db, 'comments');

  return new Promise((resolve, reject) => {
    onValue(
      commentsRef,
      (snapshot) => {
        const comments = [];
        snapshot.forEach((childSnapshot) => {
          comments.push(childSnapshot.val());
        });

        resolve(comments);
      },
      (error) => {
        reject(error);
      }
    );
  });
};
