import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyClVd-h3fUeMP-mXEWVuLGFf1VK-Rq1qcU',
  authDomain: 'forum-testing-995f7.firebaseapp.com',
  projectId: 'forum-testing-995f7',
  storageBucket: 'forum-testing-995f7.appspot.com',
  messagingSenderId: '18879388481',
  appId: '1:18879388481:web:fa31de1babbafcbc5b1fc0',
  databaseURL:
    'https://forum-testing-995f7-default-rtdb.europe-west1.firebasedatabase.app',
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);

export const storage = getStorage(app);
