import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { getAllPosts } from '../../services/post.service';
import { getAllUsers } from '../../services/post.service';
import SignUpModal from '../../Views/SignUp/SignUpModal';

export default function WelcomeSectionTest() {
  const [postCount, setPostCount] = useState(0);
  const [userCount, setUserCount] = useState(0);

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const allPosts = await getAllPosts();
        setPostCount(allPosts.length);
      } catch (error) {
        console.error('Error fetching posts:', error);
      }
    };

    const fetchUsers = async () => {
      try {
        const allUsers = await getAllUsers();

        setUserCount(allUsers.length);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchPosts();
    fetchUsers();
  }, []);

  const [openSignupModal, setOpenSignupModal] = useState(false);

  const handleOpenSignupModal = () => {
    setOpenSignupModal(true);
  };

  return (
    <main className='mt-32 mx-auto max-w-7xl px-4 '>
      <div className='text-center'>
        <h1 className='text-4xl tracking-tight font-extrabold text-gray-900 max-sm:text-3xl text-center align-middle'>
          <span className='block text-5xl xl:inline max-md:text-3xl'>
            {' '}
            Welcome to
          </span>{' '}
          <span className='block text-5xl text-indigo-600 xl:inline max-md:text-3xl'>
            DevHub
          </span>
          {' - '}
          <span className='block text-5xl xl:inline max-md:text-3xl'>
            Where Coders Unite!
          </span>
        </h1>
        <p className=' max-w-md mx-auto text-2xl text-gray-500 mt-4 max-md:text-lg md:max-w-3xl'>
          Connect with a Thriving Community of{' '}
          <span className=' text-white text-2xl px-2 rounded-md bg-purple-500 text-center max-md:text-lg'>
            {userCount}
          </span>{' '}
          Coders <br />
          Explore{' '}
          <span className=' text-white text-2xl px-2 rounded-md bg-pink-500 text-center max-md:text-lg'>
            {postCount}
          </span>{' '}
          Engaging Posts and Discussions
          <br /> Unleash Your Coding Potential - Together, We Code the Future!
        </p>

        <div className='mt-5 max-w-md mx-auto sm:flex sm:justify-center md:mt-8'>
          <div className='rounded-md shadow'>
            <button
              onClick={handleOpenSignupModal}
              className='w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10 max-md:text-sm'
            >
              JOIN NOW
            </button>
            <SignUpModal open={openSignupModal} setOpen={setOpenSignupModal} />
          </div>
        </div>
      </div>
    </main>
  );
}
