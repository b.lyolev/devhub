import React, { useState } from 'react';
import { Box, Button, Heading, Text, Tag } from '@chakra-ui/react';
import SignupModal from '../SignupModal/SignupModal';

export default function WelcomeSection() {
  const [showModal, setShowModal] = useState(false);

  const handleJoinClick = () => {
    setShowModal(true); // Open signup modal
  };

  return (
    <>
      <div className='flex justify-center text-center p-10'>
        <Box>
          <Heading color={'white'} mb={4}>
            Welcome to DevHub - Where Coders Unite!
          </Heading>

          <Text fontSize='2xl' color={'white'}>
            {/* Text content */}
          </Text>

          <Button
            h={'48px'}
            w={'150px'}
            fontSize={'xl'}
            color={'white'}
            bgGradient={'linear(to-r, #be5eff, #5a34ff)'}
            mt='24px'
            boxShadow={'dark-lg'}
            _hover={{
              transform: 'translateY(-2px)',
            }}
            _active={{
              transform: 'translateY(0px)',
            }}
            onClick={handleJoinClick}
          >
            JOIN NOW
          </Button>
        </Box>
      </div>
      {showModal && <SignupModal />}
    </>
  );
}
