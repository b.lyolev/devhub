/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  Button,
  useDisclosure,
} from "@chakra-ui/react";
import Login from "../../Views/Login/Login";

const LoginOverlay = () => (
  <ModalOverlay
    bg="blackAlpha.300"
    backdropFilter="blur(10px) "
  />
);

export default function LoginModal() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [overlay, setOverlay] = useState(<LoginOverlay />);

  return (
    <>
      <Button
        onClick={() => {
          setOverlay(<LoginOverlay />);
          onOpen();
        }}
      >
        Log In
      </Button>
      <Modal isCentered isOpen={isOpen} onClose={onClose} size="lg">
        {overlay}
        <ModalContent>
          <ModalCloseButton />
          <ModalBody>
            <Login />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
