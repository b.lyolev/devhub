/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  useDisclosure,
} from "@chakra-ui/react";
import Login from "../../Views/Login/Login";


const LoginOverlay = () => (
  <ModalOverlay
    bg="blackAlpha.300"
    backdropFilter="blur(10px)"
  />
);

export default function LoginModalRegistered() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [overlay, setOverlay] = useState(<LoginOverlay />);

  return (
    <>
      <a
        href="#"
        onClick={() => {
          setOverlay(<LoginOverlay />);
          onOpen();
        }}
        className="font-medium text-indigo-600 hover:text-indigo-500"
      >
        Log In
      </a>
      <Modal isCentered isOpen={isOpen} onClose={onClose} size="xl">
        {overlay}
        <ModalContent>
          <ModalCloseButton />
          <ModalBody>
            <Login />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
