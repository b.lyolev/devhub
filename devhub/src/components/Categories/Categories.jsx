import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { categories } from '../common/categories';
import AuthContext from '../../context/AuthContext';

const Categories = ({ onSelectCategory }) => {
  const [showDropdown, setShowDropdown] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState('All Posts');

  const handleCategoryClick = (categoryName) => {
    if (categoryName === 'All Posts') {
      onSelectCategory(null);
    } else {
      onSelectCategory(categoryName);
    }
    setSelectedCategory(categoryName);
    setShowDropdown(false);
  };

  return (
    <div className='relative inline '>
      <div className='hidden md:block'>
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 gap-0.5 mx-5'>
          {categories.slice(1).map((category) => (
            <div key={category.id} className='pt-1 mr-1 mb-1'>
              <button
                onClick={() => handleCategoryClick(category.name)}
                className={`border border-gray-200 p-3 rounded-lg hover:shadow-md flex items-center w-full shadow-inner 
                ${
                  selectedCategory === category.name
                    ? 'bg-indigo-600 text-white '
                    : 'bg-slate-100'
                }`}
              >
                <FontAwesomeIcon
                  icon={category.icon}
                  size='sm'
                  className='mr-2'
                />
                <h2 className='text-sm'>{category.name}</h2>
              </button>
            </div>
          ))}
        </div>
      </div>

      <button
        onClick={() => setShowDropdown(!showDropdown)}
        className={`border border-gray-200 p-3 rounded-sm hover:shadow-md items-center w-full shadow-inner 
        ${
          selectedCategory === categories[0].name
            ? 'bg-gray-600 text-white '
            : 'bg-slate-100'
        } md:hidden`}
      >
        <span className='mr-2'>
          <FontAwesomeIcon
            icon={categories[0].icon}
            size='sm'
            className='mr-2'
          />
        </span>
        <span className='text-md'>{categories[0].name}</span>
      </button>

      {showDropdown && (
        <div className='relative top-full left-0 mt-2 z-10 md:hidden'>
          <div className='mx-5 bg-white border border-gray-200 rounded-lg shadow-md'>
            {categories.slice(1).map((category) => (
              <button
                key={category.id}
                onClick={() => handleCategoryClick(category.name)}
                className={`border border-gray-200 p-3 rounded-sm hover:shadow-md items-center w-full shadow-inner bg-slate-100 md:hidden ${
                  selectedCategory === category.name
                    ? 'bg-indigo-600 text-white'
                    : ''
                }`}
              >
                <FontAwesomeIcon
                  icon={category.icon}
                  size='sm'
                  className='mr-2'
                />
                <span className='text-sm'>{category.name}</span>
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default Categories;
