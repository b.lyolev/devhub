import { useState, useEffect } from 'react';
import Post from '../Post/Post';
import { categories } from '.././common/categories';
import { getAllPosts } from '../../services/post.service';
import { formatDate, formatTime } from '../common/formatDateTime';
import { tagsColors } from '../../Data/tagsColors';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Top10() {
  const [topPopularPosts, setTopPopularPosts] = useState([]);
  const [topCommentedPosts, setTopCommentedPosts] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const allPosts = await getAllPosts();

        const sortedByViews = [...allPosts].sort((a, b) => b.views - a.views);
        const sortedByComments = [...allPosts].sort((a, b) => {
          const commentsA = a.comments ? Object.keys(a.comments).length : 0;
          const commentsB = b.comments ? Object.keys(b.comments).length : 0;
          return commentsB - commentsA;
        });

        setTopPopularPosts(sortedByViews.slice(0, 10));
        setTopCommentedPosts(sortedByComments.slice(0, 10));
      } catch (error) {
        console.error('Error fetching posts:', error);
      }
    };

    fetchPosts();
  }, []);

  const [tabs, setTabs] = useState([
    { name: 'TOP 10 MOST POPULAR', href: '#', current: true },
    { name: 'TOP 10 MOST COMMENTED', href: '#', current: false },
  ]);

  const [activeTab, setActiveTab] = useState(
    tabs.find((tab) => tab.current).name
  );

  const handleTabChange = (tabName, e) => {
    e.preventDefault();
    setActiveTab(tabName);

    const updatedTabs = tabs.map((tab) => ({
      ...tab,
      current: tab.name === tabName,
    }));

    setTabs(updatedTabs);
  };
  return (
    <div className='max-w-7xl items-center my-0 mx-auto pt-4'>
      <div className='p-5'>
        <div className='sm:hidden mb-5'>
          <label htmlFor='tabs' className='sr-only'>
            Select a tab
          </label>
          {/* Use an "onChange" listener to change the active tab */}
          <select
            id='tabs'
            name='tabs'
            className='text-xl block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md'
            value={activeTab} // Use value instead of defaultValue
            onChange={(e) => handleTabChange(e.target.value)}
          >
            {tabs.map((tab) => (
              <option key={tab.name} value={tab.name}>
                {tab.name}
              </option>
            ))}
          </select>
        </div>
        <div className=''>
          <div className='max-sm:hidden'>
            <nav
              className='relative z-0 rounded-lg shadow flex divide-x divide-gray-400 mb-5'
              aria-label='Tabs'
            >
              {tabs.map((tab, tabIdx) => (
                <a
                  key={tab.name}
                  href={tab.href}
                  className={classNames(
                    tab.current
                      ? 'text-gray-900'
                      : 'text-gray-500 hover:text-gray-700',
                    tabIdx === 0 ? 'rounded-l-lg' : '',
                    tabIdx === tabs.length - 1 ? 'rounded-r-lg' : '',
                    'group relative min-w-0 flex-1 overflow-hidden bg-white py-4 px-4 text-xl max-md:text-lg font-bold text-center hover:bg-gray-100 focus:z-10',
                    activeTab === tab.name ? 'bg-indigo-100' : '' // Add this line to highlight the active tab
                  )}
                  aria-current={tab.current ? 'page' : undefined}
                  onClick={(e) => handleTabChange(tab.name, e)} // Use onClick to change the active tab
                >
                  <span>{tab.name}</span>
                  <span
                    aria-hidden='true'
                    className={classNames(
                      tab.current ? 'bg-indigo-500' : 'bg-transparent',
                      'absolute inset-x-0 bottom-0 h-1'
                    )}
                  />
                </a>
              ))}
            </nav>
          </div>

          {/* Content for "TOP 10 MOST POPULAR" tab */}
          {activeTab === 'TOP 10 MOST POPULAR' && (
            <div className='flex flex-col gap-4'>
              <div className='flex flex-col gap-4'>
                {topPopularPosts.length === 0 ? (
                  <h1 className='font-medium  text-gray-700 text-lg ml-5'>
                    No posts available
                  </h1>
                ) : (
                  topPopularPosts.map((post, index) => {
                    return (
                      <Post
                        key={post.uid}
                        uid={post.uid}
                        title={post.title}
                        authorName={post.author}
                        authorUID={post.authorUID}
                        date={formatDate(post.createdOn.dateTime)}
                        time={formatTime(post.createdOn.dateTime)}
                        tags={post.tags.split(',')}
                        tagsColors={tagsColors}
                        categories={categories}
                        category={post.category}
                        commentCount={
                          post.comments ? Object.keys(post.comments).length : 0
                        }
                        likeCount={
                          post.likedBy ? Object.keys(post.likedBy).length : 0
                        }
                        viewCount={post.views || 0}
                        likedBy={post.likedBy}
                      />
                    );
                  })
                )}
              </div>
            </div>
          )}

          {/* Content for "TOP 10 MOST COMMENTED" tab */}
          {activeTab === 'TOP 10 MOST COMMENTED' && (
            <div className='flex flex-col gap-4'>
              <div className='flex flex-col gap-4'>
                {topCommentedPosts.length === 0 ? (
                  <h1 className='font-medium text-lg ml-5'>
                    No posts available
                  </h1>
                ) : (
                  topCommentedPosts.map((post, index) => {
                    return (
                      <Post
                        key={post.uid}
                        uid={post.uid}
                        title={post.title}
                        authorName={post.author}
                        authorUID={post.authorUID}
                        date={formatDate(post.createdOn.dateTime)}
                        time={formatTime(post.createdOn.dateTime)}
                        tags={post.tags.split(',')}
                        tagsColors={tagsColors}
                        categories={categories}
                        category={post.category}
                        commentCount={
                          post.comments ? Object.keys(post.comments).length : 0
                        }
                        likeCount={
                          post.likedBy ? Object.keys(post.likedBy).length : 0
                        }
                        viewCount={post.views || 0}
                        likedBy={post.likedBy}
                      />
                    );
                  })
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
