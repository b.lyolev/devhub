import React, { useState, useEffect, useContext } from 'react';
import AuthContext from '../../context/AuthContext';
import { sortPosts } from '../../utils/sortPosts';
import { sortOptions } from '../../utils/sortPosts';
import Post from '../Post/Post';
import { tagsColors } from '../../Data/tagsColors';
import { getPostsByCategory } from '../../services/post.service';
import { categories } from '.././common/categories';
import { getUserData } from '../../services/user.service';
import { formatDate, formatTime } from '../common/formatDateTime';
import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';
import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  useMediaQuery,
} from '@chakra-ui/react';
import { ChevronDownIcon } from '@heroicons/react/outline';

export default function Posts({ category }) {
  const { user } = useContext(AuthContext);
  const [posts, setPosts] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [selectedSort, setSelectedSort] = useState("newest");
  const [isBigger] = useMediaQuery("(min-width:768px)");
  const [currentPage, setCurrentPage] = useState(1);
  const postsPerPage = 10;

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const snapshot = await getUserData(user.uid);
        if (snapshot.exists()) {
          const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
          setCurrentUser(userData);
        } else {
          console.log("User doesn't exist");
        }
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    fetchUserData();
  }, [user.uid]);

  useEffect(() => {
    if (user === null) return alert('Please login to view posts');

    const updatePosts = (newPosts) => {
      setPosts(newPosts) || [];
    };

    getPostsByCategory(category, updatePosts);
  }, [user, category]);

  const handleSortClick = (sortOption) => {
    setSelectedSort(sortOption);
  };

  const sortedPosts = sortPosts(posts, selectedSort);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = sortPosts(sortedPosts, selectedSort).slice(
    indexOfFirstPost,
    indexOfLastPost
  );

  const totalPages = Math.ceil(
    sortPosts(sortedPosts, selectedSort).length / postsPerPage
  );

  const handlePageChange = (page) => {
    setCurrentPage(page);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <>
      <div className='flex justify-between '>
        <div className='pt-5 pl-10'>
          {selectedSort === 'newest' && (
            <span
              style={{ color: 'black', fontWeight: 'bold', fontSize: '22px' }}
            >
              Newest Posts
            </span>
          )}
          {selectedSort === 'oldest' && (
            <span
              style={{ color: 'black', fontWeight: 'bold', fontSize: '22px' }}
            >
              Oldest Posts
            </span>
          )}
          {selectedSort === 'likes' && (
            <span
              style={{ color: 'black', fontWeight: 'bold', fontSize: '22px' }}
            >
              Most Liked Posts
            </span>
          )}
          {selectedSort === 'comments' && (
            <span
              style={{ color: 'black', fontWeight: 'bold', fontSize: '22px' }}
            >
              Most Commented Posts
            </span>
          )}
        </div>
        <div className='relative pt-5 pr-10 inline-block'>
          <Menu placement={isBigger ? 'bottom-end' : 'bottom-start'}>
            <MenuButton
              as='button'
              className='rounded bg-transparent focus:outline-none focus:border-indigo-500'
            >
              <div className='relative pt-1 inline-flex items-center'>
                <span
                  style={{
                    color: 'black',
                    fontWeight: '500',
                    fontSize: '19px',
                  }}
                >
                  Sort
                </span>
                <ChevronDownIcon className='ml-1 h-5 w-5 text-gray-500' />
              </div>
            </MenuButton>
            <MenuList>
              {sortOptions.map((option) => (
                <MenuItem
                  key={option.value}
                  onClick={() => handleSortClick(option.value)}
                >
                  {option.label}
                </MenuItem>
              ))}
            </MenuList>
          </Menu>
        </div>
      </div>

      <div className='items-center my-0 mx-auto pt-5'>
        <div className=''>
          <div className='px-5'>
            {currentPosts.length === 0 ? (
              <h1 className='font-medium  text-gray-700 text-lg ml-5'>
                No posts available
              </h1>
            ) : (
              currentPosts.map((post, index) => {
                return (
                  <div key={post.uid} className='mb-5'>
                    <Post
                      uid={post.uid}
                      title={post.title}
                      authorName={post.author}
                      authorUID={post.authorUID}
                      date={formatDate(post.createdOn.dateTime)}
                      time={formatTime(post.createdOn.dateTime)}
                      tags={post.tags.split(',')}
                      tagsColors={tagsColors}
                      categories={categories}
                      category={post.category}
                      commentCount={
                        post.comments ? Object.keys(post.comments).length : 0
                      }
                      likeCount={
                        post.likedBy ? Object.keys(post.likedBy).length : 0
                      }
                      viewCount={post.views || 0}
                      likedBy={post.likedBy}
                    />
                  </div>
                );
              })
            )}
            <div className='flex justify-center mt-5'>
              {totalPages > 2 && currentPage > 1 && (
                <button
                  className={`mx-1 p-1 text-xl font-bold bg-gray-100 hover:bg-gray-200 text-gray-600 rounded-lg focus:outline-none focus:ring transition-colors`}
                  onClick={() => handlePageChange(currentPage - 1)}
                >
                  <ChevronLeftIcon boxSize={6} />
                </button>
              )}
              {Array.from({ length: totalPages }).map((_, index) => {
                if (
                  (index < 2 && currentPage <= 2) ||
                  (index >= currentPage - 1 && index <= currentPage)
                ) {
                  return (
                    <button
                      key={index}
                      className={`mx-1 p-2 text-lg font-medium ${
                        currentPage === index + 1
                          ? 'bg-indigo-500 text-white'
                          : 'bg-gray-100 hover:bg-gray-200 text-gray-600'
                      } rounded-lg focus:outline-none focus:ring transition-colors`}
                      onClick={() => handlePageChange(index + 1)}
                      style={{ minWidth: '40px' }}
                    >
                      {index + 1}
                    </button>
                  );
                } else if (index === 2 && currentPage > 3) {
                  return <span key={index}></span>;
                }
                return null;
              })}
              {totalPages > 2 && currentPage < totalPages && (
                <button
                  className={`mx-1 p-1 text-lg font-bold bg-gray-100 hover:bg-gray-200 text-gray-600 rounded-lg focus:outline-none focus:ring transition-colors`}
                  onClick={() => handlePageChange(currentPage + 1)}
                >
                  <ChevronRightIcon boxSize={6} />
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
