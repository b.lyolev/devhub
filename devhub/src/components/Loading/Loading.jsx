const LoadingSpinner = () => {
  return (
    <div className='flex justify-center items-center py-4'>
      <div className='border-t-4 border-blue-500 rounded-full animate-spin h-12 w-12'></div>
    </div>
  );
};

export default LoadingSpinner;
