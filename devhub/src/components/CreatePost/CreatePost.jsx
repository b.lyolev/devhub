import { Fragment, useContext, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../../context/AuthContext';
import { createPostHandle } from '../../services/post.service';
import { getUserData } from '../../services/user.service';
import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';
import { MultiSelect } from 'react-multi-select-component';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { tags } from '../common/tags';
import { categories } from '../common/categories';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function CreatePost() {
  const [selectedTags, setSelectedTags] = useState([]);
  const [form, setForm] = useState({
    author: '',
    title: '',
    content: '',
  });
  const [category, setCategory] = useState(categories[0]);
  const [currentUser, setCurrentUser] = useState(null);
  const { user } = useContext(AuthContext);

  const [author, setAuthor] = useState('');

  useEffect(() => {
    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
        setCurrentUser(userData);
        setAuthor(`${userData.firstName} ${userData.lastName}`);
      })
      .catch((e) => alert(e.message));
  }, [user.uid]);

  const onCreatePost = (e) => {
    e.preventDefault();
    if (currentUser && currentUser.isBlocked) {
      return alert('You are blocked and cannot create a post.');
    }

    if (!form.title) {
      return alert('Please enter title!');
    }

    // The title must be between 16 and 64 symbols.
    if (form.title.length < 16 || form.title.length > 64) {
      return alert('Title must be between 16 and 64 symbols!');
    }

    if (!form.content) {
      return alert('Please enter content!');
    }
    // The content must be between 32 symbols and 8192 symbols.
    if (form.content.length < 32 || form.content.length > 8192) {
      return alert('Content must be between 32 symbols and 8192 symbols!');
    }

    if (category.name === 'Select a category') {
      return alert('Please select category!');
    }

    createPostHandle(
      currentUser.handle,
      user.uid,
      author,
      form.title,
      form.content,
      category.name,

      selectedTags.map((tag) => tag.value).join(',')
    )
      .then(() => {
        console.log('Post Created');

        setForm({
          author: '',
          title: '',
          content: '',
        });

        setSelectedTags([]);

        setCategory(categories[0]);
      })
      .catch((e) => console.log(e));
  };

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  return (
    <div>
      {currentUser && currentUser.isBlocked && (
        <p className='absolute h-32  p-12 top-6 right-0 left-0 bottom-0 items-center justify-center  z-10 max-sm:text-base text-xl text-red-500 font-bold text-center '>
          <FontAwesomeIcon
            className='mr-4 text-3xl max-sm:text-xl '
            icon={faExclamationTriangle}
          />
          You are currently blocked and cannot create a post.
        </p>
      )}
      <form
        action='#'
        className={`relative mx-5 ${
          currentUser && currentUser.isBlocked
            ? 'filter blur-2 opacity-70 pointer-events-none transition duration-300 select-none'
            : ''
        }`}
      >
        <div className='  px-2 bg-slate-50 rounded-2xl shadow-sm overflow-hidden focus-within:border-indigo-500 focus-within:ring-1 focus-within:ring-indigo-500 '>
          <label htmlFor='title' className='sr-only'>
            Title
          </label>
          <input
            type='text'
            name='title'
            id='title'
            className='block w-full  bg-slate-50 border-0 pt-2.5  text-lg font-medium placeholder-gray-500 focus:ring-0'
            placeholder='Title'
            value={form.title}
            onChange={updateForm('title')}
          />
          <label htmlFor='content' className='sr-only'>
            Description
          </label>
          <textarea
            rows={2}
            name='content'
            id='content'
            className='block w-full border-0 bg-slate-50 h-24 mb-3 resize-none placeholder-gray-500 focus:ring-0 sm:text-sm'
            placeholder='Write a description...'
            value={form.content}
            onChange={updateForm('content')}
          />

          <div aria-hidden='true'>
            <div className='h-px' />
            <div className='py-2 '>
              <div className='py-px'>
                <div className='h-9' />
              </div>
            </div>
          </div>
        </div>

        <div className='absolute bottom-0 inset-x-px'>
          <div className='border-t border-gray-200 mx-1 px-2 py-3 flex  items-center space-x-3 sm:px-3'>
            <div className='flex-1'>
              <div className='flex items-center gap-2'>
                <Listbox value={category} onChange={setCategory}>
                  {({ open }) => (
                    <>
                      <div className=' relative'>
                        <Listbox.Button className='  relative w-max max-sm:w-16  bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-pointer focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 text-base'>
                          <span className='flex items-center '>
                            <FontAwesomeIcon icon={category.icon} />
                            <span className='ml-3 block truncate'>
                              {category.name}
                            </span>
                          </span>
                          <span className='ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none'>
                            <SelectorIcon
                              className='h-5 w-5 text-gray-400 '
                              aria-hidden='true'
                            />
                          </span>
                        </Listbox.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          leave='transition ease-in duration-100'
                          leaveFrom='opacity-100'
                          leaveTo='opacity-0'
                        >
                          <Listbox.Options className=' absolute z-10 mt-1 w-full max-sm:overflow-x-hidden bg-white shadow-lg max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none '>
                            {categories.slice(1).map((category) => (
                              <Listbox.Option
                                key={category.id}
                                className={({ active }) =>
                                  classNames(
                                    active
                                      ? 'text-white bg-indigo-600'
                                      : 'text-gray-900',
                                    'cursor-pointer select-none relative py-2 pl-3 pr-9'
                                  )
                                }
                                value={category}
                              >
                                {({ selected, active }) => (
                                  <>
                                    <div className='flex items-center'>
                                      <FontAwesomeIcon icon={category.icon} />
                                      <span
                                        className={classNames(
                                          selected
                                            ? 'font-semibold'
                                            : 'font-normal',
                                          'ml-3 block truncate'
                                        )}
                                      >
                                        {category.name}
                                      </span>
                                    </div>

                                    {selected ? (
                                      <span
                                        className={classNames(
                                          active
                                            ? 'text-white'
                                            : 'text-indigo-600',
                                          'absolute inset-y-0 right-0 flex items-center pr-4 max-sm:hidden'
                                        )}
                                      >
                                        <CheckIcon
                                          className='h-5 w-5'
                                          aria-hidden='true'
                                        />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </>
                  )}
                </Listbox>
                <MultiSelect
                  className='transition-all w-64 max-md:w-36 lg:w-80 '
                  options={tags}
                  value={selectedTags}
                  onChange={setSelectedTags}
                  labelledBy='Select'
                />
              </div>
            </div>

            <div className='flex-shrink-0'>
              <button
                type='submit'
                className='inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                onClick={onCreatePost}
              >
                Create Post
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
