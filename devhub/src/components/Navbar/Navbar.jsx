import { Fragment, useState, useContext, useRef, useEffect } from 'react';
import { Disclosure, Menu, Transition } from '@headlessui/react';
import { SearchIcon } from '@heroicons/react/solid';
import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline';
import { NavLink, Router, useNavigate } from 'react-router-dom';
import AuthContext from '../../context/AuthContext';
import { logoutUser } from '../../services/auth.service';
import { getUserData } from '../../services/user.service';
import { set } from 'firebase/database';
import LoginModal from '../../Views/Login/LoginModal';
import SignUpModal from '../../Views/SignUp/SignUpModal';
import { Link } from 'react-router-dom';
import SearchBar from '../SearchBar/SearchBar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faLaptopCode,
  faMicrochip,
  faUserSecret,
} from '@fortawesome/free-solid-svg-icons';
import { Avatar } from '@chakra-ui/react';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Navbar() {
  const [openLoginModal, setOpenLoginModal] = useState(false);
  const [openSignupModal, setOpenSignupModal] = useState(false);

  const handleOpenLoginModal = () => {
    setOpenLoginModal(true);
  };

  const handleOpenSignupModal = () => {
    setOpenSignupModal(true);
  };

  const { user, userData, setUser } = useContext(AuthContext);

  const [username, setUsername] = useState('');

  const [currentUser, setCurrentUser] = useState({});

  const [role, setRole] = useState('');

  const [message, setMessage] = useState('');

  const [updated, setUpdated] = useState('');

  const inputRef = useRef(null);

  const navigateTo = useNavigate();

  const [dropdownVisible, setDropdownVisible] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState('');

  const toggleDropdown = () => {
    setDropdownVisible(!dropdownVisible);
  };

  const handleCategoryClick = (categoryName) => {
    setSelectedCategory(categoryName);
    toggleDropdown();
  };

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setUsername(snapshot.val()[Object.keys(snapshot.val())[0]].handle);
        const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
        setCurrentUser(userData);

        setRole(snapshot.val()[Object.keys(snapshot.val())[0]].role || 'User');
      })
      .catch((e) => alert(e.message));
  }, [user]);

  const onLogout = () => {
    logoutUser().then(() => {
      setUser({
        user: null,
        userData: null,
      });
    });

    setOpenLoginModal(false);
    setOpenSignupModal(false);

    navigateTo('/');
  };

  return (
    <Disclosure
      as='nav'
      className='bg-gradient-to-tr from-gray-800 via-gray-900 to-black fixed top-0 left-0 w-full p-2 z-50 border-b-2 border-gray-700'
    >
      {({ open }) => (
        <>
          <div className='max-w-7xl mx-auto px-2 sm:px-4 lg:px-8 '>
            <div className='relative flex items-center justify-between h-16'>
              <div className='flex items-center px-2 lg:px-0'>
                <Link to='/'>
                  <div className='flex items-center justify-center  bg-indigo-600 rounded-lg px-2 py-1 max-sm:p-0 max-sm:bg-transparent  text-center align-middle '>
                    {/* <img
                      className='block  h-12 w-auto '
                      src='./src/images/logo.png'
                      alt='DevHub Logo'
                    /> */}
                    <FontAwesomeIcon
                      className='py-2 px-1 h-8 w-auto text-white max-sm:h-8'
                      icon={faLaptopCode}
                    />
                    <div className=' text-white text-3xl font-bold ml-2  max-sm:hidden'>
                      DevHub
                    </div>
                  </div>
                </Link>
              </div>

              <div className='relative flex items-center'>
                <div className='w-full'>
                  <SearchBar />
                </div>
              </div>

              <div className='flex lg:hidden'>
                {/* Mobile menu button */}
                <Disclosure.Button className='inline-flex items-center justify-center p-1 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white'>
                  <span className='sr-only'>Open main menu</span>
                  {open ? (
                    <XIcon className='block h-6 w-6' aria-hidden='true' />
                  ) : (
                    <MenuIcon className='block h-6 w-6' aria-hidden='true' />
                  )}
                </Disclosure.Button>
              </div>
              <div className='hidden lg:block '>
                <div className='flex items-center'>
                  <div>
                    {user === null ? (
                      <>
                        <div className='ml-6 space-x-3'>
                          <button
                            onClick={handleOpenLoginModal}
                            className='inline-block bg-white py-2 px-4 border border-transparent rounded-md text-base font-medium text-indigo-600 hover:bg-indigo-50'
                          >
                            Sign in
                          </button>
                          <LoginModal
                            open={openLoginModal}
                            setOpen={setOpenLoginModal}
                          />
                          <button
                            onClick={handleOpenSignupModal}
                            className='inline-block bg-indigo-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75'
                          >
                            Sign up
                          </button>
                          <SignUpModal
                            open={openSignupModal}
                            setOpen={setOpenSignupModal}
                          />
                        </div>
                      </>
                    ) : (
                      <>
                        {/* <button
                          type='button'
                          className='flex-shrink-0 bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white'
                        >
                          <span className='sr-only'>View notifications</span>
                          <BellIcon className='h-6 w-6' aria-hidden='true' />
                        </button> */}
                        {/* Profile dropdown */}
                        <Menu as='div' className='ml-4 relative flex-shrink-0'>
                          <div>
                            <Menu.Button className='items-center bg-gray-800/40 p-1 rounded-full flex text-sm text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white'>
                              <span className='sr-only'>Open user menu</span>
                              <div className='NameOfUser mx-2 text-right px-1'>
                                <span className='text-base font-semibold tracking-tight text-white'>
                                  {username}
                                </span>
                                <p className='text-sm font-semibold  text-indigo-400'>
                                  {/* Moderator */}
                                  {/* Admin */}
                                  {role}
                                </p>
                              </div>
                              <Avatar
                                size='md'
                                className='ring-2 ring-indigo-600'
                                name={
                                  currentUser?.firstName &&
                                  currentUser?.lastName
                                    ? currentUser.firstName +
                                      ' ' +
                                      currentUser.lastName
                                    : ''
                                }
                                src={currentUser?.profileImage}
                              />
                            </Menu.Button>
                          </div>
                          <Transition
                            as={Fragment}
                            enter='transition ease-out duration-100'
                            enterFrom='transform opacity-0 scale-95'
                            enterTo='transform opacity-100 scale-100'
                            leave='transition ease-in duration-75'
                            leaveFrom='transform opacity-100 scale-100'
                            leaveTo='transform opacity-0 scale-95'
                          >
                            <Menu.Items className='origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none'>
                              <Menu.Item>
                                {({ active }) => (
                                  <Link
                                    to='/profile'
                                    className={classNames(
                                      active ? 'bg-gray-100' : '',
                                      'block px-4 py-2 text-sm text-gray-700'
                                    )}
                                  >
                                    Your Profile
                                  </Link>
                                )}
                              </Menu.Item>
                              {/* <Menu.Item>
                                {({ active }) => (
                                  <Link
                                    to='/settings'
                                    className={classNames(
                                      active ? 'bg-gray-100' : '',
                                      'block px-4 py-2 text-sm text-gray-700'
                                    )}
                                  >
                                    Settings
                                  </Link>
                                )}
                              </Menu.Item> */}
                              <Menu.Item>
                                {({ active }) => (
                                  <a
                                    href='#'
                                    onClick={onLogout}
                                    className={classNames(
                                      active ? 'bg-gray-100' : '',
                                      'block px-4 py-2 text-sm text-gray-700'
                                    )}
                                  >
                                    Sign out
                                  </a>
                                )}
                              </Menu.Item>
                            </Menu.Items>
                          </Transition>
                        </Menu>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Disclosure.Panel className='lg:hidden'>
            <div className='px-2 pt-2 pb-3 space-y-1'>
              {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
            </div>
            <div className='pt-4 pb-3 border-t border-gray-700'>
              {user === null ? (
                <div className='mt-6'>
                  <button
                    onClick={handleOpenLoginModal}
                    className='w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700'
                  >
                    Sign in
                  </button>

                  <p className='mt-6 text-center text-base font-medium text-gray-500'>
                    Don't have an account yet?{' '}
                    <button
                      onClick={handleOpenSignupModal}
                      className='text-indigo-600 hover:text-indigo-500'
                    >
                      Sign up
                    </button>
                  </p>
                </div>
              ) : (
                <div>
                  <div className='flex items-center px-5'>
                    <div className='flex-shrink-0'>
                    <Avatar
                                size='md'
                                className='ring-2 ring-indigo-600'
                                name={
                                  currentUser?.firstName &&
                                  currentUser?.lastName
                                    ? currentUser.firstName +
                                      ' ' +
                                      currentUser.lastName
                                    : ''
                                }
                                src={currentUser?.profileImage}
                              />
                    </div>

                    <div className='NameOfUser mx-2 text-left px-1'>
                      <span className='text-base font-semibold tracking-tight text-white'>
                        
                        {username}
                      </span>
                      <p className='text-sm font-semibold  text-indigo-400'>
                        {/* Moderator */}
                        {/* Admin */}
                        {role}
                      </p>
                    </div>
                  </div>
                  <div className='mt-3 px-2 space-y-1'>
                    <Disclosure.Button
                      as='a'
                      href='/profile'
                      className='block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700'
                    >
                      Your Profile
                    </Disclosure.Button>

                    <Disclosure.Button
                      as='a'
                      href='#'
                      onClick={onLogout}
                      className='block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700'
                    >
                      Sign out
                    </Disclosure.Button>
                  </div>
                </div>
              )}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}
