import { QuestionMarkCircleIcon } from "@heroicons/react/solid";
import { useContext, useEffect, useState } from "react";
import { Avatar } from "@chakra-ui/react";
import {
  updateUserProfile,
  uploadImageAndSaveLink,
} from '../../services/user.service';
import AuthContext from '../../context/AuthContext';
import { getUserData } from '../../services/user.service';
import { set } from 'firebase/database';
import LoadingSpinner from '../Loading/Loading';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export default function ProfileEdit() {
  const { user } = useContext(AuthContext);

  const [selectedImage, setSelectedImage] = useState(null);

  const [isLoading, setIsLoading] = useState(true);

  const [isSavingPhoto, setIsSavingPhoto] = useState(false);

  useEffect(() => {
    if (user !== null) {
      const fetchUserData = async () => {
        try {
          const snapshot = await getUserData(user.uid);
          if (snapshot.exists()) {
            const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
            setCurrentUser(userData);
          } else {
            console.log("User doesn't exist");
          }
        } catch (error) {
          console.error("Error fetching user data:", error);
        } finally {
          setIsLoading(false);
        }
      };

      fetchUserData();
    }
  }, [user]);

  const [currentUser, setCurrentUser] = useState("");
  useEffect(() => {
    setSelectedImage(currentUser.profileImage);
  }, [currentUser.profileImage]);

  const [form, setForm] = useState("");

  useEffect(() => {
    if (currentUser) {
      setForm({
        selectedFile: null,
        aboutMe: currentUser.aboutMe || "",
        firstName: currentUser.firstName || "",
        lastName: currentUser.lastName || "",
        email: currentUser.email || "",
        city: currentUser.city || "",
        password: "",
        confirmPassword: "",
      });
    }
  }, [currentUser]);

  const [validationMessages, setValidationMessages] = useState({
    firstName: "",
    lastName: "",
    email: "",
    city: "",
    password: "",
    confirmPassword: "",
  });

  const findDifferences = () => {
    const differences = {};
    for (const key in form) {
      if (
        form[key] !== currentUser[key] &&
        key !== "selectedFile" &&
        key !== "confirmPassword" &&
        form[key] !== ""
      ) {
        differences[key] = form[key];
      }
    }
    return differences;
  };

  const updateForm = (prop) => (e) => {
    const value = e.target.value;

    setForm({
      ...form,
      [prop]: value,
    });

    const newValidationMessages = { ...validationMessages };

    switch (prop) {
      case "firstName":
        newValidationMessages.firstName =
          value.length < 4 || value.length > 32
            ? "First name must be between 4 and 32 characters!"
            : "";
        if (!/^[A-Za-z]+$/.test(value)) {
          newValidationMessages.firstName =
            "First name can only contain letters!";
        }
        break;
      case "lastName":
        newValidationMessages.lastName =
          value.length < 4 || value.length > 32
            ? "Last name must be between 4 and 32 characters!"
            : "";
        if (!/^[A-Za-z\s]+$/.test(value)) {
          newValidationMessages.lastName =
            "Last name can only contain letters and spaces!";
        }
        break;
      case "email":
        newValidationMessages.email = !value
          ? "Please enter an email!"
          : !/^\S+@\S+\.\S+$/.test(value)
          ? "Please enter a valid email address!"
          : "";
        break;
      case "city":
        newValidationMessages.city = !value ? "Please select a city!" : "";
        break;
      case "password":
        newValidationMessages.password =
          value.length < 7
            ? "Password should be at least 7 characters long!"
            : "";

        if (!/[A-Za-z]/.test(value)) {
          newValidationMessages.password =
            "Password should contain at least one letter!";
        } else if (!/\d/.test(value)) {
          newValidationMessages.password =
            "Password should contain at least one digit!";
        } else if (!/[A-Z]/.test(value)) {
          newValidationMessages.password =
            "Password should contain at least one uppercase letter!";
        } else if (!/[!@#$%^&.*]/.test(value)) {
          newValidationMessages.password =
            "Password should contain at least one special character!";
        }

        if (form.confirmPassword !== "" && value !== form.confirmPassword) {
          newValidationMessages.confirmPassword = "Passwords do not match!";
        } else {
          newValidationMessages.confirmPassword = "";
        }
        break;
      case "confirmPassword":
        newValidationMessages.confirmPassword =
          value !== form.password ? "Passwords do not match!" : "";

        break;
      default:
        break;
    }

    setValidationMessages(newValidationMessages);
  };

  const [isNewPhotoSelected, setIsNewPhotoSelected] = useState(false);

  const handleFileChange = (e) => {
    const selectedFile = e.target.files[0];
    setForm({
      ...form,
      selectedFile: selectedFile,
    });

    setIsNewPhotoSelected(true);
    setSelectedImage(URL.createObjectURL(selectedFile));
  };

  const handlePhotoSave = async () => {
    if (form.selectedFile) {
      setIsSavingPhoto(true);

      try {
        console.log("Selected file detected:", form.selectedFile.name);
        const imageUrl = await uploadImageAndSaveLink(
          currentUser.handle,
          form.selectedFile
        );

        setSelectedImage(URL.createObjectURL(form.selectedFile));
        setIsNewPhotoSelected(false);
        setIsSavingPhoto(false); // Stop loading
        console.log("Profile picture updated successfully");
      } catch (error) {
        setIsSavingPhoto(false); // Stop loading
        console.error("Error updating profile picture:", error);
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const differences = findDifferences();

    if (Object.keys(differences).length > 0) {
      console.log("Changed fields:", differences);

      try {
        await updateUserProfile(currentUser.handle, differences);

        if (!currentUser.city) {
          differences.city = form.city;
        }
        if (!currentUser.aboutMe) {
          differences.aboutMe = form.aboutMe;
        }

        if (Object.keys(differences).length > 0) {
          await updateUserProfile(currentUser.handle, differences);
          console.log(differences);
          console.log("Profile updated successfully");
        } else {
          console.log("No changes to update");
        }

        toast.success('Profile updated successfully');
      } catch (error) {
        console.error('Error updating profile:', error);
        toast.error('An error occurred while updating the profile');
      }
    } else {
      console.log("No changes to update");
    }
  };
  return (
    <>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <section aria-labelledby='profile-details-heading'>
          <ToastContainer position='bottom-right' autoClose={2000} />

          <form>
            <div className="shadow sm:rounded-md sm:overflow-hidden">
              <div className="bg-white py-6 px-4 sm:p-6">
                <div>
                  <h2
                    id="profile-details-heading"
                    className="text-lg leading-6 font-medium text-gray-900"
                  >
                    Profile details
                  </h2>
                  <p className="mt-1 text-sm text-gray-500">
                    Update your profile information. If your email address
                    changes, we’ll send you a confirmation message.
                  </p>
                </div>

                <div className="mt-6 grid grid-cols-4 gap-6">
                  <div className="col-span-4 sm:col-span-2">
                    <label className="block text-sm font-medium  mb-3 text-gray-700">
                      Photo
                    </label>
                    <div className="mt-1 flex items-end">
                      <Avatar
                        size="2xl"
                        name={
                          currentUser.firstName + " " + currentUser.lastName
                        }
                        src={selectedImage || currentUser.profileImage}
                      />

                      <label
                        htmlFor="image-upload"
                        className="ml-5 bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 cursor-pointer"
                      >
                        Change
                      </label>
                      {isNewPhotoSelected && (
                        <button
                          type="button"
                          className="ml-2 bg-indigo-600 border border-gray-300 rounded-md shadow-sm py-2 px-3 text-sm leading-4 font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 cursor-pointer"
                          onClick={handlePhotoSave}
                          disabled={isSavingPhoto}
                        >
                          {isSavingPhoto ? "Saving..." : "Save Photo"}
                        </button>
                      )}
                      <input
                        type="file"
                        id="image-upload"
                        accept="image/*"
                        className="hidden"
                        onChange={handleFileChange}
                      />
                    </div>
                  </div>
                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="about-me"
                      className="block text-sm font-medium text-gray-700"
                    >
                      About me
                    </label>
                    <textarea
                      type="text"
                      name="about-me"
                      id="about-me"
                      placeholder={
                        currentUser.aboutMe || "Write something about you..."
                      }
                      className="mt-1  block w-full h-36 max-h-36 border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm"
                      value={currentUser.aboutMe}
                      onChange={updateForm("aboutMe")}
                    />
                  </div>
                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="first-name"
                      className="block text-sm font-medium text-gray-700"
                    >
                      First name
                    </label>
                    <input
                      type="text"
                      name="first-name"
                      id="first-name"
                      autoComplete="cc-given-name"
                      placeholder={currentUser.firstName}
                      className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm'
                      value={form.firstName}
                      onChange={updateForm('firstName')}
                    />
                    {validationMessages.firstName && (
                      <p className="text-red-500 text-xs mt-1">
                        {validationMessages.firstName}
                      </p>
                    )}
                  </div>

                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="last-name"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Last name
                    </label>
                    <input
                      type="text"
                      name="last-name"
                      id="last-name"
                      autoComplete="cc-family-name"
                      placeholder={currentUser.lastName}
                      className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm'
                      value={form.lastName}
                      onChange={updateForm('lastName')}
                    />
                    {validationMessages.lastName && (
                      <p className="text-red-500 text-xs mt-1">
                        {validationMessages.lastName}
                      </p>
                    )}
                  </div>

                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="email-address"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Email address
                    </label>
                    <input
                      type="text"
                      name="email-address"
                      id="email-address"
                      autoComplete="email"
                      placeholder={currentUser.email}
                      className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm'
                      value={form.email}
                      onChange={updateForm('email')}
                    />
                    {validationMessages.email && (
                      <p className="text-red-500 text-xs mt-1">
                        {validationMessages.email}
                      </p>
                    )}
                  </div>

                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="country"
                      className="block text-sm font-medium text-gray-700"
                    >
                      City
                    </label>
                    <select
                      id='country'
                      name='country'
                      autoComplete='country-name'
                      className='mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm'
                      value={form.city}
                      onChange={updateForm('city')}
                    >
                      <option value="">Select a city</option>
                      <option value="Plovdiv">Plovdiv</option>
                      <option value="Sofia">Sofia</option>
                      <option value="Varna">Varna</option>
                      <option value="Burgas">Burgas</option>
                      <option value="Ruse">Ruse</option>
                      <option value="Stara Zagora">Stara Zagora</option>
                      <option value="Pleven">Pleven</option>
                      <option value="Sliven">Sliven</option>
                      <option value="Dobrich">Dobrich</option>
                      <option value="Shumen">Shumen</option>
                      <option value="Pernik">Pernik</option>
                    </select>

                    {validationMessages.city && (
                      <p className="text-red-500 text-xs mt-1">
                        {validationMessages.city}
                      </p>
                    )}
                  </div>

                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="password"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Password
                    </label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      placeholder="********"
                      className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm"
                      onChange={updateForm("password")}
                    />
                    {validationMessages.password && (
                      <p className="text-red-500 text-xs mt-1">
                        {validationMessages.password}
                      </p>
                    )}
                  </div>
                  <div className="col-span-4 sm:col-span-2">
                    <label
                      htmlFor="password-confirm"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Confirm Password
                    </label>
                    <input
                      type="password"
                      name="password"
                      id="password-confirm"
                      placeholder="********"
                      className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm"
                      onChange={updateForm("confirmPassword")}
                    />
                    {validationMessages.confirmPassword && (
                      <p className="text-red-500 text-xs mt-1">
                        {validationMessages.confirmPassword}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button
                  disabled={Object.values(validationMessages).some(
                    (message) => message !== ""
                  )}
                  onClick={handleSubmit}
                  type="submit"
                  className={`justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 
                  ${
                    Object.values(validationMessages).some(
                      (message) => message !== ""
                    )
                      ? "bg-indigo-300 cursor-not-allowed"
                      : "bg-indigo-600 hover:bg-indigo-700"
                  }`}
                >
                  Save
                </button>
              </div>
            </div>
          </form>
        </section>
      )}
    </>
  );
}
