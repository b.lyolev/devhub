import { ChatAlt2Icon, CreditCardIcon } from '@heroicons/react/solid';
import { useEffect, useState } from 'react';
import {
  blockUser,
  getAllUsers,
  unblockUser,
} from '../../services/post.service';
import { Avatar } from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faComment,
  faHeart,
  faSignsPost,
  faUser,
  faUserAltSlash,
  faWindowRestore,
} from '@fortawesome/free-solid-svg-icons';
import { formatDate, formatTime } from '../common/formatDateTime';

const people = [
  {
    name: 'Jane Cooper',
    title: 'Regional Paradigm Technician',
    role: 'Admin',
    email: 'janer@example.com',
    telephone: '+1-202-555-0170',
    imageUrl:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
];

export default function AdminUsers({ user }) {
  const [allUsers, setAllUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const [searchTerm, setSearchTerm] = useState('');
  const [sortOption, setSortOption] = useState('recentlyCreated');
  const [filteredUsers, setFilteredUsers] = useState([]);

  useEffect(() => {
    if (user.role.toLowerCase() === 'admin') {
      const fetchUsers = async () => {
        try {
          const users = await getAllUsers();

          setAllUsers(users);
        } catch (error) {
          console.error('Error fetching user data:', error);
        } finally {
          setIsLoading(false);
        }
      };

      fetchUsers();
    }
  }, [user, user.isBlocked]);

  useEffect(() => {
    // Filter and sort users initially when allUsers changes
    const updatedFilteredUsers = filterAndSortUsers(
      allUsers,
      searchTerm,
      sortOption
    );
    setFilteredUsers(updatedFilteredUsers);
  }, [allUsers, searchTerm, sortOption]);

  const handleSortOptionChange = (e) => {
    const newSortOption = e.target.value;
    setSortOption(newSortOption);
  };

  const handleSearchTermChange = (e) => {
    const newSearchTerm = e.target.value;
    setSearchTerm(newSearchTerm);
  };
  const filterAndSortUsers = (users, searchTerm, sortOption) => {
    let filteredUsers = users.filter(
      (user) =>
        user.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
        user.lastName.toLowerCase().includes(searchTerm.toLowerCase())
    );

    switch (sortOption) {
      case 'mostPosts':
        filteredUsers.sort((a, b) => {
          const aCreatedPostsCount = Object.keys(a.createdPosts || {}).length;
          const bCreatedPostsCount = Object.keys(b.createdPosts || {}).length;
          return bCreatedPostsCount - aCreatedPostsCount;
        });
        break;
      case 'mostComments':
        filteredUsers.sort((a, b) => {
          const aCommentsCount = Object.keys(a.comments || {}).length;
          const bCommentsCount = Object.keys(b.comments || {}).length;
          return bCommentsCount - aCommentsCount;
        });
        break;
      case 'recentlyCreated':
        filteredUsers.sort(
          (a, b) =>
            new Date(b.createdOn.dateTime) - new Date(a.createdOn.dateTime)
        );
        break;
      case 'oldest':
        filteredUsers.sort(
          (a, b) =>
            new Date(a.createdOn.dateTime) - new Date(b.createdOn.dateTime)
        );
        break;
      default:
      // No sorting
    }
    return filteredUsers;
  };

  const handleBlockUser = async (userHandle, isBlocked) => {
    try {
      if (isBlocked) {
        await unblockUser(userHandle);
      } else {
        await blockUser(userHandle);
      }

      // Update the user's isBlocked property in the state
      setAllUsers((prevUsers) =>
        prevUsers.map((user) =>
          user.handle === userHandle ? { ...user, isBlocked: !isBlocked } : user
        )
      );
    } catch (error) {
      console.error('Error handling user block/unblock:', error);
    }
  };

  return (
    <div className='p-4'>
      {/* Search bar */}
      <div className='mb-4 flex justify-between items-center  gap-3 max-sm:flex-col'>
        <input
          type='text'
          placeholder='Search users...'
          value={searchTerm}
          onChange={handleSearchTermChange}
          className='px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-blue-200 w-full '
        />
        <select
          value={sortOption}
          onChange={handleSortOptionChange}
          className='px-4 py-2 border border-gray-300 rounded-md  min-w-[200px] focus:outline-none focus:ring focus:ring-blue-200 max-sm:w-full '
        >
          <option value='none'>Sort By</option>
          <option value='mostPosts'>Most Posts</option>
          <option value='mostComments'>Most Comments</option>
          <option value='recentlyCreated'>Recently Created</option>
          <option value='oldest'>Oldest</option>
        </select>
      </div>
      <ul
        role='list'
        className='grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2'
      >
        {filteredUsers.map((user) => (
          <li
            key={user.uid}
            className='col-span-1 flex flex-col  bg-white rounded-lg shadow divide-y divide-gray-200'
          >
            <div className='flex-1 flex flex-row p-4  '>
              <div className=' flex flex-col text-center justify-between'>
                <Avatar
                  size='xl'
                  className='ring-2 ring-indigo-600 '
                  name={user.firstName + ' ' + user.lastName}
                  src={user.profileImage}
                />
                <div>
                  <h3 className='mt-2 text-gray-900 text-sm font-medium'>
                    <p>{user.firstName} </p>
                    <p>{user.lastName}</p>
                  </h3>
                  <div className='mt-1 px-1.5 py-0.5 text-slate-800 text-sm font-medium bg-green-300 rounded-full'>
                    {user.role || 'User'}
                  </div>
                </div>
              </div>
              <dl className=' ml-4 flex flex-col gap-1 justify-between w-full'>
                <div>
                  <dt className='sr-only'>Account Created on</dt>
                  <dd className='font-medium  text-black text-sm leading-4'>
                    Account Created on:
                    <p className='text-base font-normal'>
                      {`${formatDate(user.createdOn.dateTime)} 
                    ${formatTime(user.createdOn.dateTime)}`}
                    </p>
                  </dd>
                  <dt className='sr-only'>About me</dt>
                  <dd className='font-medium  text-black text-sm'>
                    About:
                    <p className='text-base font-normal'>
                      {user.aboutMe || 'No description provided'}
                    </p>
                  </dd>
                  <dt className='sr-only'>Email</dt>
                  <dd className='font-medium  text-black text-sm'>
                    Email:
                    <p className='text-base font-normal'>{user.email}</p>
                  </dd>
                </div>
                <div className='flex flex-row gap-2 justify-end mt-3 text-xs font-bold'>
                  <div className='w-14 py-1.5 px-1.5   rounded-md bg-indigo-200  text-center'>
                    <span className='pr-1'>
                      {user.createdPosts
                        ? Object.keys(user.createdPosts).length
                        : 0}
                    </span>

                    <FontAwesomeIcon
                      className='text-xs'
                      icon={faWindowRestore}
                    ></FontAwesomeIcon>
                  </div>
                  <div className=' w-11 py-1.5 px-1.5 rounded-md bg-indigo-200 text-center'>
                    <span className='pr-1'>
                      {user.comments ? Object.keys(user.comments).length : 0}
                    </span>
                    <FontAwesomeIcon
                      className='text-xs'
                      icon={faComment}
                    ></FontAwesomeIcon>
                  </div>
                  <div className='w-11 py-1.5 px-1.5   rounded-md bg-indigo-200  text-center'>
                    <span className='pr-1'>
                      {user.likedBy ? Object.keys(user.likedBy).length : 0}
                    </span>
                    <FontAwesomeIcon
                      className='text-xs'
                      icon={faHeart}
                    ></FontAwesomeIcon>
                  </div>
                </div>
              </dl>
            </div>
            <div>
              <div className='-mt-px flex divide-x divide-gray-200'>
                <div className='w-0 flex-1 flex '>
                  <a className='relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500'>
                    <CreditCardIcon
                      className='w-5 h-5 text-gray-400'
                      aria-hidden='true'
                    />
                    <span className='ml-3'>Posts</span>
                  </a>
                </div>
                <div className='-ml-px w-0 flex-1 flex '>
                  <a className='relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500'>
                    <ChatAlt2Icon
                      className='w-5 h-5 text-gray-400'
                      aria-hidden='true'
                    />
                    <span className='ml-3'>Comments</span>
                  </a>
                </div>

                <div className='-ml-px w-0 flex-1 flex '>
                  <button
                    disabled={user.role?.toLowerCase() === 'admin'}
                    onClick={() => handleBlockUser(user.handle, user.isBlocked)}
                    className={`relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm font-medium border border-transparent ${
                      user.role?.toLowerCase() === 'admin'
                        ? 'bg-gray-200 text-gray-500 cursor-not-allowed'
                        : user.isBlocked
                        ? ' text-red-500 hover:text-green-500 '
                        : ' text-gray-700  hover:text-red-500'
                    } `}
                  >
                    <FontAwesomeIcon
                      icon={user.isBlocked ? faUserAltSlash : faUser}
                      className={`mr-1 `}
                    />
                    <span className='ml-1'>
                      {user.isBlocked ? 'Unblock' : 'Block User'}
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
