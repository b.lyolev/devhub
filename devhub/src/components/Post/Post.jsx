import { useEffect } from 'react';
import './Post.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPenToSquare,
  faEye,
  faHeart,
  faComment,
  faTrashCan,
} from '@fortawesome/free-solid-svg-icons';
import { Avatar } from '@chakra-ui/react';
import { css } from '@emotion/css';
import { useState, useContext } from 'react';

import { Link, useNavigate } from 'react-router-dom';
import AuthContext from '../../context/AuthContext';
import LoginModal from '../../Views/Login/LoginModal';
import {
  deletePostAndRelatedData,
  togglePostLike,
} from '../../services/post.service';
import { getUserData } from '../../services/user.service';
import Alert from '../Alert/Alert';
import LoadingSpinner from '../Loading/Loading';

export default function Post({
  uid,
  title,
  authorName,
  authorUID,
  date,
  time,
  tags,
  tagsColors,
  categories,
  category,
  commentCount,
  likeCount,
  viewCount,
  likedBy,
}) {
  const { user } = useContext(AuthContext);

  const [isYourPost, setIsYourPost] = useState(false);

  const [currentUser, setCurrentUser] = useState(null);

  const [author, setAuthor] = useState(null);

  const [openDeleteAlert, setOpenDeleteAlert] = useState(false);

  const [currentUserLoading, setCurrentUserLoading] = useState(true);

  const [liked, setLiked] = useState(false);

  const handleDeleteAlertOpen = () => {
    setOpenDeleteAlert(true);
  };

  useEffect(() => {
    if (user !== null) {
      const fetchUserData = async () => {
        try {
          const snapshot = await getUserData(user.uid);
          if (snapshot.exists()) {
            const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
            setCurrentUser(userData);

            if (userData.createdPosts && userData.createdPosts[uid]) {
              setIsYourPost(true);
            } else {
              setIsYourPost(false);
            }
          } else {
            console.log("User doesn't exist");
          }
        } catch (error) {
          console.error('Error fetching user data:', error);
        } finally {
          setCurrentUserLoading(false);
        }
      };

      fetchUserData();

      if (likedBy && likedBy[user.uid]) {
        setLiked(true);
      }
    }
  }, [user, uid, likedBy]);

  // i will make use effect to get the userdata

  useEffect(() => {
    getUserData(authorUID).then((snapshot) => {
      if (snapshot.exists()) {
        const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
        setAuthor(userData);
      } else {
        console.log("User doesn't exist");
      }
    });
  }, [authorUID]);

  const handleLikeClick = async () => {
    if (user === null) return;
    try {
      const isLiked = await togglePostLike(
        user.uid,
        uid,
        currentUser.handle,
        liked
      );
      setLiked(isLiked);
    } catch (error) {
      console.error('Error handling like:', error);
    }
  };

  const [openLoginModal, setOpenLoginModal] = useState(false);

  const navigate = useNavigate();

  const handleTitleClick = () => {
    if (!user) {
      setOpenLoginModal(true);
    } else {
      navigate.push(`/post/${uid}`);
    }
  };

  const handleDeleteClick = async () => {
    if (user === null) return;
    try {
      const isDeleted = await deletePostAndRelatedData(uid);

      if (isDeleted) {
        navigate('/');
      } else {
        console.log('Post not found.');
      }
    } catch (error) {
      console.error('Error deleting post:', error);
    }
  };

  const selectedCategory = categories.find(
    (currentCategory) => currentCategory.name === category
  );

  return (
    <div className='Post backdrop-blur-md bg-blue-100  shadow-md hover:bg-indigo-50 transition-all duration-500'>
      <>
        <div className='h-36 w-44 border-4 border-slate-600 bg-opacity-75 bg-gray-200 shadow-md rounded-lg text-center p-8 text-7xl max-md:hidden flex justify-center items-center'>
          <FontAwesomeIcon icon={selectedCategory.icon} />
        </div>

        <div className='PostContent '>
          <div className='PostTitleWithButtons mb-3 '>
            <Link
              to={user ? `/post/${uid}` : null}
              onClick={user ? null : handleTitleClick}
              className='text-2xl font-bold break-words text-black max-md:text-2xl max-sm:text-xl transition-all cursor-pointer'
            >
              {title}
            </Link>
            <LoginModal open={openLoginModal} setOpen={setOpenLoginModal} />

            <div className='PostButtons flex align-middle gap-2 '>
              {(isYourPost ||
                (currentUser &&
                  currentUser.role?.toLowerCase() === 'admin')) && (
                <div>
                  <button
                    onClick={handleDeleteAlertOpen}
                    className='flex items-center gap-1 rounded-lg bg-gray-800 shadow-md px-2.5 py-2.5 m-auto text-sm font-semibold text-gray-100 cursor-pointer hover:bg-indigo-600 duration-300 max-md:text-md max-sm:text-sm max-md:p-1.5'
                  >
                    <FontAwesomeIcon icon={faTrashCan} />
                  </button>
                  <Alert
                    open={openDeleteAlert}
                    setToFalse={() => setOpenDeleteAlert(false)}
                    handleConfirmDelete={handleDeleteClick} // Pass the delete function
                  />
                </div>
              )}
            </div>
            {/* )} */}
          </div>

          <div className='flex  flex-wrap gap-1 text-lg font-medium mb-5 mt-1'>
            {/* {console.log(tags)} */}
            {tags.length !== 1 ? (
              tags.map((tag, index) => (
                <span
                  key={index}
                  className={css`
           background-color: ${tagsColors[tag]}0.7);
           border: 1px solid ${tagsColors[tag]}0.8);
           display: block;
           padding: 2px 6px;
           font-size: 1rem;
           line-height: 18px;
           color: #fff;
           border-radius: 4px;
             cursor: pointer;
             transition: 'all';         
             transition-duration: 200;
             &:hover {
               background-color: ${tagsColors[tag]}0.4);
               color: black;
               border: 1px solid ${tagsColors[tag]}0.8);
               transition-duration: 200;

             }
           `}
                >
                  {tag}
                </span>
              ))
            ) : (
              <div className='text-base  text-white ring-1 ring-black bg-gray-500 rounded-md  px-1'>
                No tags selected
              </div>
            )}
          </div>

          <div className='PostInfo'>
            <div className='flex items-center gap-2 cursor-pointer'>
              <div className='PostAuthorPhoto max-sm:hidden'>
                <Avatar
                  size='md'
                  name={authorName}
                  src={author?.profileImage}
                />
              </div>
              <div className='flex flex-col '>
                <div className='text-lg font-bold  max-md:text-sm'>
                  {authorName}
                </div>
                <div className='text-md font-medium max-md:text-sm'>
                  {date} | {time}
                </div>
              </div>
            </div>

            <div className='flex gap-2 text-xl font-bold mr-1 '>
              <div className='gap-1 flex  items-center px-2.5 py-0.5 rounded-md text-base  bg-slate-200 max-md:text-md hover:bg-purple-300 duration-300 cursor-pointer max-md:text-lg max-sm:text-sm '>
                <span>{viewCount}</span>
                <FontAwesomeIcon icon={faEye} />
              </div>
              <div
                className='gap-1 flex items-center px-2.5 py-0.5 rounded-md text-base font-bold bg-slate-200 max-md:text-md hover:bg-purple-300 duration-300 cursor-pointer max-md:text-lg max-sm:text-sm '
                onClick={handleLikeClick}
              >
                <span>{likeCount}</span>
                <FontAwesomeIcon
                  className={`text-${
                    liked ? 'red-500' : 'gray-400'
                  } hover:scale-125 duration-300`}
                  icon={faHeart}
                />
              </div>
              <div className='gap-1 flex  items-center px-2.5  py-0.5 rounded-md text-base font-bold bg-slate-200 max-md:text-md hover:bg-purple-300 duration-300 cursor-pointer max-md:text-lg max-sm:text-sm '>
                <span>{commentCount}</span>
                <div>
                  <FontAwesomeIcon icon={faComment} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </div>
  );
}
