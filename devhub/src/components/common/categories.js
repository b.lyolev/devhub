import {
  faArrowRight,
  faBook,
  faBriefcase,
  faCode,
  faComments,
  faDesktop,
  faGlobe,
  faMobile,
  faNewspaper,
  faProjectDiagram,
  faRobot,
} from '@fortawesome/free-solid-svg-icons';

export const categories = [
  { id: 0, name: 'Select a category', icon: faArrowRight },
  {
    id: 1,
    name: 'General Discussion',
    icon: faGlobe,
  },
  {
    id: 2,
    name: 'Programming Languages',
    icon: faCode,
  },
  {
    id: 3,
    name: 'Data Science and ML',
    icon: faRobot,
  },
  {
    id: 4,
    name: 'Web Development',
    icon: faDesktop,
  },
  {
    id: 5,
    name: 'Mobile Development',
    icon: faMobile,
  },

  {
    id: 6,
    name: 'Career and Jobs',
    icon: faBriefcase,
  },

  {
    id: 7,
    name: 'Tutorials and Resources',
    icon: faBook,
  },

  {
    id: 8,
    name: 'Off-Topic',
    icon: faComments,
  },

  {
    id: 9,
    name: 'News and Announcements',
    icon: faNewspaper,
  },

  {
    id: 10,
    name: 'All Posts',
    icon: faArrowRight,
  },
];
