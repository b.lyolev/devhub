export function formatTime(dateTime) {
  const dateObject = new Date(dateTime);
  let hours = dateObject.getHours();
  let minutes = dateObject.getMinutes();

  if (hours === 24) {
    hours = 0;
  }

  const formattedHours = hours.toString().padStart(2, '0');
  const formattedMinutes = minutes.toString().padStart(2, '0');

  return `${formattedHours}:${formattedMinutes}`;
}

export function formatDate(dateTime) {
  return new Date(dateTime).toLocaleDateString('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  });
}

export function formatRelativeTime(dateTime) {
  const now = new Date();
  const diffInSeconds = Math.floor((now - new Date(dateTime)) / 1000);

  if (diffInSeconds < 60) {
    return `${diffInSeconds} seconds ago`;
  } else if (diffInSeconds < 3600) {
    const minutes = Math.floor(diffInSeconds / 60);
    return `${minutes} ${minutes === 1 ? 'minute' : 'minutes'} ago`;
  } else if (diffInSeconds < 86400) {
    const hours = Math.floor(diffInSeconds / 3600);
    return `${hours} ${hours === 1 ? 'hour' : 'hours'} ago`;
  } else if (diffInSeconds < 604800) {
    const days = Math.floor(diffInSeconds / 86400);
    return `${days} ${days === 1 ? 'day' : 'days'} ago`;
  } else {
    const weeks = Math.floor(diffInSeconds / 604800);
    const remainingDays = Math.floor((diffInSeconds % 604800) / 86400);
    return `${weeks} ${weeks === 1 ? 'week' : 'weeks'} and ${remainingDays} ${
      remainingDays === 1 ? 'day' : 'days'
    } ago`;
  }
}
