import React from 'react';
import { Box, chakra, Container, Grid, Text, useColorModeValue } from '@chakra-ui/react';
import { faLaptopCode } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <Box bg={useColorModeValue('transparent')} py={4}>
      <Container maxW="full">
        <Grid
          templateColumns={{ base: '1fr', md: '1fr auto 1fr' }}
          alignItems="center"
          justifyContent="center" // Center horizontally
          textAlign={{ base: 'center', md: 'left' }}
          px={{ base: 4, md: 8 }}
        >
          <div /> {/* Empty div to create space on the left */}
          <Text color={useColorModeValue('gray.600', 'gray.400')}>
            © {currentYear}{' '}
            <FontAwesomeIcon
              className='mx-auto h-6 w-auto align-middle'
              icon={faLaptopCode}
            />{' '}
            DevHub. All rights reserved.
          </Text>
          <div /> {/* Empty div to create space on the right */}
        </Grid>
      </Container>
    </Box>
  );
};

export default Footer;
