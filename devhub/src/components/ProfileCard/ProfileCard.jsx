import { useContext } from 'react';
import AuthContext from '../../context/AuthContext';

import { Link } from 'react-router-dom';

const ProfileCard = () => {
  const { userData } = useContext(AuthContext);

  // Ensure userData is defined and not null
  if (!userData) return <p>Loading user data...</p>;

  return (
    <div className="profile-card">
      <h2>Your Profile</h2>

      <div className="user-info">
        <p><strong>Name:</strong> {userData.name}</p>
        <p><strong>Email:</strong> {userData.email}</p>
        <p><strong>Role:</strong> {userData.role}</p>
      </div>

      <Link to="/profile/edit">Edit Profile</Link>
    </div>
  );
  // return <div>ProfileCard</div>
}

export default ProfileCard;