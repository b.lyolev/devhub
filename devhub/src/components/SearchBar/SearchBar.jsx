import React, { useContext, useState, useRef, useEffect } from 'react';
import { ref, query, orderByChild, equalTo, get } from 'firebase/database';
import { NavLink } from 'react-router-dom';
import { SearchIcon } from '@heroicons/react/solid';
import { db } from '../../config/firebase-config';
import AuthContext from '../../context/AuthContext';
import LoginModal from '../../Views/Login/LoginModal';
import './SearchBar.css';

function SearchBar() {
  const { user } = useContext(AuthContext);
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [openLoginModal, setOpenLoginModal] = useState(false);

  const suggestionRef = useRef(null);

  useEffect(() => {
    document.addEventListener('click', handleDocumentClick);

    return () => {
      document.removeEventListener('click', handleDocumentClick);
    };
  }, []);

  const handleDocumentClick = (event) => {
    if (
      suggestionRef.current &&
      !suggestionRef.current.contains(event.target)
    ) {
      setIsDropdownOpen(false);
    }
  };

  const handleOpenLoginModal = () => {
    setOpenLoginModal(true);
  };

  const handleChange = (event) => {
    const value = event.target.value;
    setSearchQuery(value);
    setIsDropdownOpen(value !== '');
    if (value.trim() !== '') {
      handleSearch();
    } else {
      setSearchResults([]);
    }
  };

  const handleKeyDown = async (event) => {
    if (event.key === 'Enter') {
      try {
        const postsRef = ref(db, 'posts');
        const querySnapshot = await get(postsRef);

        const results = [];
        querySnapshot.forEach((postSnapshot) => {
          const post = postSnapshot.val();
          if (post.title.toLowerCase().includes(searchQuery.toLowerCase())) {
            results.push({ uid: postSnapshot.key, ...post });
          }
        });

        console.log('Query Results:', results);
        setSearchResults(results);
        setIsDropdownOpen(true);
      } catch (error) {
        console.error('Error searching for posts:', error);
      }
    }
  };

  const handleSearch = async () => {
    try {
      const postsRef = ref(db, 'posts');
      const querySnapshot = await get(postsRef);

      const results = [];
      querySnapshot.forEach((postSnapshot) => {
        const post = postSnapshot.val();
        if (post.title.toLowerCase().includes(searchQuery.toLowerCase())) {
          results.push({ uid: postSnapshot.key, ...post });
        }
      });

      console.log('Query Results:', results);
      setSearchResults(results);
      setIsDropdownOpen(true);
    } catch (error) {
      console.error('Error searching for posts:', error);
    }
  };

  const handlePostClick = () => {
    setIsDropdownOpen(false);
    setSearchQuery('');
  };

  return (
    <div className='relative'>
      <div className='relative  flex items-center'>
        <input
          id='search'
          name='search'
          value={searchQuery}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          className='pl-10 pr-3 py-2 border border-transparent rounded-md bg-gray-600 text-black placeholder-slate-400 focus:outline-none focus:bg-white focus:border-white focus:ring-white text-base sm:text-sm shadow-sm w-full md:w-96'
          placeholder='Search'
          autoComplete='off'
          type='search'
        />

        <div className='absolute inset-y-0  left-0 pl-3 flex items-center'>
          <SearchIcon
            className='h-5 w-5 text-gray-400'
            aria-hidden='true'
            onClick={handleSearch}
          />
        </div>
      </div>

      {user && isDropdownOpen && searchResults.length > 0 && (
        <div
          ref={suggestionRef}
          className='absolute mt-2 z-10 w-full max-h-60 overflow-y-auto border-2 rounded-md border-gray-800'
        >
          <ul className='bg-slate-200 shadow-md divide-y divide-gray-300 p-3'>
            {searchResults.slice(0, 10).map((post) => (
              <li key={post.uid} className='p-2'>
                <NavLink
                  to={`/post/${post.uid}`}
                  className='text-black'
                  onClick={handlePostClick}
                >
                  {post.title}
                </NavLink>
              </li>
            ))}
          </ul>
        </div>
      )}
      {!user && isDropdownOpen && (
        <div
          ref={suggestionRef}
          className='absolute mt-2 z-10 w-full bg-white p-4 rounded-md shadow-md'
        >
          <p className='text-gray-600'>
            Please
            <span className='px-1  text-indigo-600 font-bold'>sign in</span>
            to see search results.
          </p>
        </div>
      )}
    </div>
  );
}

export default SearchBar;
