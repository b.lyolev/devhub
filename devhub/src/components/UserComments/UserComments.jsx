import { faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import {
  deleteCommentByUID,
  getAllCommentsByUserHandle,
} from '../../services/post.service';
import { formatDate, formatTime } from '../common/formatDateTime';
import LoadingSpinner from '../Loading/Loading';
import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  useMediaQuery,
} from '@chakra-ui/react';
import { ChevronDownIcon } from '@chakra-ui/icons';
import { commentSortOptions } from '../../utils/sortPosts';

export default function UserComments({ user }) {
  const [isLoadingUser, setIsLoadingUser] = useState(true);
  const [isLoadingComments, setIsLoadingComments] = useState(false);
  const [selectedCommentSort, setSelectedCommentSort] = useState('newest');
  const [isBigger] = useMediaQuery('(min-width:768px');
  const [sortedComments, setSortedComments] = useState([]);
  const [comments, setComments] = useState([]);

  useEffect(() => {
    if (user === null) return;

    setIsLoadingComments(true);

    getAllCommentsByUserHandle(user.handle)
      .then((userComments) => {
        setComments(userComments);
      })
      .catch((error) => {
        console.error('Error fetching user comments:', error);
      })
      .finally(() => {
        setIsLoadingComments(false);
      });
  }, [user]);

  useEffect(() => {
    if (comments.length === 0) return;

    const sortedUserComments = sortComments(comments, selectedCommentSort);
    setSortedComments(sortedUserComments);
  }, [comments, selectedCommentSort]);

  function sortComments(comments, sortOption) {
    let sortedComments = [...comments];

    if (sortOption === 'newest') {
      sortedComments.sort(
        (a, b) =>
          new Date(b.createdOn.dateTime) - new Date(a.createdOn.dateTime)
      );
    } else if (sortOption === 'oldest') {
      sortedComments.sort(
        (a, b) =>
          new Date(a.createdOn.dateTime) - new Date(b.createdOn.dateTime)
      );
    }

    return sortedComments;
  }

  const handleDeleteComment = async (comment) => {
    try {
      await deleteCommentByUID(
        comment.uid,
        comment.authorUID,
        comment.postUID,
        user.handle
      );
      setComments((prevComments) =>
        prevComments.filter((c) => c.uid !== comment.uid)
      );
    } catch (error) {
      console.error('Error deleting comment:', error);
    }
  };

  return (
    <>
      {isLoadingUser && isLoadingComments ? (
        <LoadingSpinner />
      ) : (
        <section aria-labelledby='billing-history-heading'>
          <div className='bg-white pt-6 shadow sm:rounded-md sm:overflow-hidden'>
            <div className='flex justify-between items-center px-4'>
              <div className='flex items-center'>
                <h2
                  id='billing-history-heading'
                  className='text-lg leading-6 font-medium text-gray-900'
                >
                  My comments
                </h2>
              </div>
              <div className='flex justify-end'>
                <Menu placement={isBigger ? 'bottom-end' : 'bottom-start'}>
                  <MenuButton
                    as='button'
                    className='rounded bg-transparent focus:outline-none focus:border-indigo-500'
                  >
                    <div className='relative inline-flex items-center'>
                      <span
                        style={{
                          color: 'black',
                          fontWeight: '500',
                          fontSize: '16px',
                        }}
                      >
                        {
                          commentSortOptions.find(
                            (option) => option.value === selectedCommentSort
                          ).label
                        }
                      </span>
                      <ChevronDownIcon className='ml-1 h-5 w-5 text-gray-500' />
                    </div>
                  </MenuButton>
                  <MenuList placement='right'>
                    {commentSortOptions.map((option) => (
                      <MenuItem
                        key={option.value}
                        onClick={() => setSelectedCommentSort(option.value)}
                      >
                        {option.label}
                      </MenuItem>
                    ))}
                  </MenuList>
                </Menu>
              </div>
            </div>
            <div className='mt-6 flex flex-col'>
              <div className='-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8'>
                <div className='py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8'>
                  <div className='overflow-hidden border-t border-gray-200'>
                    <table className='min-w-full divide-y divide-gray-200'>
                      <thead className='bg-gray-50'>
                        <tr>
                          <th
                            scope='col'
                            className='px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider'
                          >
                            Created on
                          </th>
                          <th
                            scope='col'
                            className='px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider'
                          >
                            Comment
                          </th>

                          <th
                            scope='col'
                            className='relative px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider'
                          >
                            <span className='sr-only'>View receipt</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody className='bg-white divide-y divide-gray-200'>
                        {sortedComments.length === 0 ? (
                          <tr>
                            <td className='px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900'>
                              No comments to display.
                            </td>
                          </tr>
                        ) : (
                          sortedComments.map((comment) => (
                            <tr key={comment.uid}>
                              <td className='px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900'>
                                {formatDate(comment.createdOn.dateTime)}
                                {' at '}
                                {formatTime(comment.createdOn.dateTime)}
                              </td>
                              <td className='px-6 py-4 whitespace-nowrap text-sm text-gray-500'>
                                {comment.content.length > 64
                                  ? comment.content.substring(0, 64) + '...'
                                  : comment.content}
                              </td>
                              <td className='px-6 py-4 whitespace-nowrap text-right text-sm font-medium'>
                                <a
                                  href={`post/${comment.postUID}`}
                                  className='text-white rounded-md bg-indigo-600 py-1 px-2 hover:bg-indigo-800 mr-3'
                                >
                                  View post
                                </a>
                                <button
                                  onClick={() => handleDeleteComment(comment)}
                                  className='text-indigo-600 hover:text-indigo-900'
                                >
                                  <FontAwesomeIcon icon={faTrashCan} /> Delete
                                </button>
                              </td>
                            </tr>
                          ))
                        )}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
}
