/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  useDisclosure,
} from "@chakra-ui/react";
import SignUp from "../../Views/SignUp/SignUp";

const SignupOverlay = () => (
  <ModalOverlay
    bg='blackAlpha.300'
    backdropFilter='blur(10px)'
  />
);

export default function SignupModalCreate() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [overlay, setOverlay] = useState(<SignupOverlay />);

  return (
    <>
      <a
        href="#"
        onClick={() => {
          setOverlay(<SignupOverlay />);
          onOpen();
        }}
        className='font-medium text-indigo-600 hover:text-indigo-500'
      >
        Create Account Now
      </a>
      <Modal isCentered 
      isOpen={isOpen} 
      onClose={onClose}
      size='xl'
      >
        {overlay}
        <ModalContent>
          <ModalCloseButton />
          <ModalBody>
            <SignUp />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
