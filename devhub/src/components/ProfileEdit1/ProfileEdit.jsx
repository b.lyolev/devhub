import { useContext } from 'react';
import AuthContext from '../../context/AuthContext';


const ProfileEdit = () => {
  const { userData } = useContext(AuthContext);

  return (
    <div className="profile-edit">
      <h2>Edit Profile</h2>
      {/* Form for editing profile */}
      <div>
        <label>Name:</label>
        <input type="text" defaultValue={userData?.name} />
        <label>Email:</label>
        <input type="email" defaultValue={userData?.email} />
        <button>Save Changes</button>
      </div>
      <h3>Your Posts:</h3>
      {/* Display user posts. Assuming posts is an array in userData. */}
      {userData?.posts?.map(post => (
        <div key={post.id}>
          <h4>{post.title}</h4>
          <p>{post.content}</p>
        </div>
      ))}
    </div>
  );
}

export default ProfileEdit;