import { faComment, faEye, faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useEffect, useState } from "react";
import { getAllPostsByUserHandle } from "../../services/post.service";
import AuthContext from "../../context/AuthContext";
import { getUserData } from "../../services/user.service";
import LoadingSpinner from "../Loading/Loading";
import { formatDate, formatTime } from "../common/formatDateTime";
import { Link } from "react-router-dom";
import { sortPosts } from "../../utils/sortPosts";
import { sortUserOptions } from "../../utils/sortPosts";
import { Menu, MenuButton, MenuList, MenuItem, useMediaQuery } from "@chakra-ui/react";
import { ChevronDownIcon } from "@heroicons/react/outline";

// const posts = [
//   {
//     id: 1,
//     title: "My first post, yay! I am so excited! I am so excited!",
//     date: "1/1/2020",
//     datetime: "2020-01-01",
//     description: "Business Plan - Annual Billing",
//     amount: "CA$109.00",
//     views: 5,
//     comments: 2,
//     likes: 3,
//     href: "#",
//   },
// ];

export default function UserPosts({ user }) {
  // const { user } = useContext(AuthContext);
  // const [currentUser, setCurrentUser] = useState(null);
  const [isLoadingUser, setIsLoadingUser] = useState(true);
  const [isLoadingPosts, setIsLoadingPosts] = useState(false);
  const [selectedSort, setSelectedSort] = useState("newest");
  const [isBigger] = useMediaQuery("(min-width:768px");
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    if (user === null) return;

    setIsLoadingPosts(true);

    getAllPostsByUserHandle(user.handle)
      .then((userPosts) => {
        setPosts(userPosts);
      })
      .catch((error) => {
        console.error("Error fetching user posts:", error);
      })
      .finally(() => {
        setIsLoadingPosts(false);
      });
  }, [user]);

  // console.log(posts);

  const handleSortClick = (sortOption) => {
    setSelectedSort(sortOption);
  };


  const sortedPosts = sortPosts(posts, selectedSort);

  return (
    <>
      {isLoadingUser && isLoadingPosts ? (
        <LoadingSpinner />
      ) : (
        <section aria-labelledby="billing-history-heading">
          <div className="bg-white pt-6 shadow sm:rounded-md sm:overflow-hidden">
            <div className="flex justify-between items-center px-4">
              <h2
                id="billing-history-heading"
                className="text-lg leading-6 font-medium text-gray-900"
              >
                My posts
              </h2>
              <div className="flex justify-end">
                <Menu placement={isBigger ? "bottom-end" : "bottom-start"}>
                  <MenuButton
                    as="button"
                    className="rounded bg-transparent focus:outline-none focus:border-indigo-500"
                  >
                    <div className="relative inline-flex items-center">
                      <span
                        style={{
                          color: "black",
                          fontWeight: "500",
                          fontSize: "16px",
                        }}
                      >
                        {sortUserOptions.find((option) => option.value === selectedSort).label}
                      </span>
                      <ChevronDownIcon className="ml-1 h-5 w-5 text-gray-500" />
                    </div>
                  </MenuButton>
                  <MenuList>
                    {sortUserOptions.map((option) => (
                      <MenuItem
                        key={option.value}
                        onClick={() => handleSortClick(option.value)}
                      >
                        {option.label}
                      </MenuItem>
                    ))}
                  </MenuList>
                </Menu>
              </div>
            </div>
            <div className="mt-6 flex flex-col">
              <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                  <div className="overflow-hidden border-t border-gray-200">
                    <table className="min-w-full divide-y divide-gray-200">
                      <thead className="bg-gray-50">
                        <tr>
                          <th
                            scope="col"
                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                          >
                            Created on
                          </th>
                          <th
                            scope="col"
                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                          >
                            Title
                          </th>
                          <th
                            scope="col"
                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                          >
                            Stats
                          </th>

                          <th
                            scope="col"
                            className="relative px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                          >
                            <span className="sr-only">View receipt</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody className="bg-white divide-y divide-gray-200">
                        {sortedPosts.length === 0 ? (
                          <tr>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                              No posts to display.
                            </td>
                          </tr>
                        ) : (
                          sortedPosts.map((post, index) => (
                            <tr key={index}>
                              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {formatDate(post.createdOn.dateTime)} at{" "}
                                {formatTime(post.createdOn.dateTime)}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {post.title.length > 64
                                  ? post.title.substring(0, 64) + "..."
                                  : post.title}
                              </td>
                              <td className="flex mt-3 whitespace-nowrap text-xs font-medium text-gray-800 gap-2">
                                <div className="w-11 py-1.5 px-1.5 rounded-sm bg-slate-300 text-center">
                                  <span className="pr-1">{post.views}</span>
                                  <FontAwesomeIcon
                                    className="text-sm"
                                    icon={faEye}
                                  />
                                </div>
                                <div className="w-11 py-1.5 px-1.5 rounded-sm bg-slate-300 text-center">
                                  <span className="pr-1">
                                    {post.comments
                                      ? Object.keys(post.comments).length
                                      : 0}
                                  </span>
                                  <FontAwesomeIcon
                                    className="text-sm"
                                    icon={faComment}
                                  />
                                </div>
                                <div className="w-11 py-1.5 px-1.5 rounded-sm bg-slate-300 text-center">
                                  <span className="pr-1">
                                    {post.likedBy
                                      ? Object.keys(post.likedBy).length
                                      : 0}
                                  </span>
                                  <FontAwesomeIcon
                                    className="text-sm"
                                    icon={faHeart}
                                  />
                                </div>
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <Link
                                  to={`/post/${post.uid}`}
                                  className="text-indigo-600 hover:text-indigo-900"
                                >
                                  View post to edit
                                </Link>
                              </td>
                            </tr>
                          ))
                        )}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
}
