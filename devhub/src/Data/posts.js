import { faCode, faRobot } from '@fortawesome/free-solid-svg-icons';

export const posts = [
  //each post must have a unique id, title,content,authorName,authorPhoto, date,tags, category, commentCount, likeCount, viewCount and isYourPost,isLiked
  {
    id: 1,
    title: 'How to use React Router',
    content:
      'React Router is a collection of navigational components that compose declaratively with your application. Whether you want to have bookmarkable URLs for your web app or a composable way to navigate in React Native, React Router works wherever React is rendering--so take your pick!',
    authorName: 'John Doe',
    authorPhoto: 'https://picsum.photos/200',
    createdOn: {
      date: '02-08-2023',
      time: '12:55 PM',
    },
    tags: ['react', 'react-router'],
    category: 'ProgrammingLanguages',
    commentCount: 2,
    likeCount: 5,
    viewCount: 10,
    isYourPost: false,
    isLiked: false,
  },
  {
    id: 2,
    title: 'Introduction to Machine Learning',
    content:
      'Machine learning is a field of computer science that uses statistical techniques to give computer systems the ability to "learn" (e.g., progressively improve performance on a specific task) from data, without being explicitly programmed.',
    authorName: 'Bozhidar Lyolev',
    authorPhoto: 'https://picsum.photos/200',
    createdOn: {
      date: '02-08-2023',
      time: '12:55 PM',
    },
    tags: ['machine learning', 'python'],
    category: 'MachineLearning',
    commentCount: 6,
    likeCount: 2,
    viewCount: 128,
    isYourPost: true,
    isLiked: true,
  },

  {
    id: 3,
    title: 'Should I use React or Angular',
    content:
      "You should use React, because it's better, and it's the future.It's also easier to learn and it's more popular.Most of the companies are using React, so you will have more job opportunities.",
    authorName: 'Mihail Mihaylov',
    authorPhoto: 'https://picsum.photos/200',
    createdOn: {
      date: '02-08-2023',
      time: '12:55 PM',
    },
    tags: ['react', 'angular', 'javascript'],
    category: 'ProgrammingLanguages',
    commentCount: 0,
    likeCount: 0,
    viewCount: 0,
    isYourPost: false,
    isLiked: false,
  },
  {
    id: 4,
    title: 'What do you think about Copilot',
    content:
      "It will replace us, the developers, and we won't have jobs anymore. We should stop using it, and we should stop using VS Code, because it's made by Microsoft, and Microsoft made Copilot.Otherwise, we will be replaced by AI.PS: It's a joke, don't take it seriously.Don't stop using VS Code, it's the best code editor.",
    authorName: 'Maria Petrova',
    authorPhoto: 'https://picsum.photos/200',
    createdOn: {
      date: '02-08-2023',
      time: '12:55 PM',
    },
    tags: ['copilot', 'ai', 'data science'],
    category: 'MachineLearning',
    commentCount: 43,
    likeCount: 10,
    viewCount: 198,
    isYourPost: true,
    isLiked: false,
  },
  {
    id: 5,
    title: 'What do you think about Copilot',
    content:
      "It will replace us, the developers, and we won't have jobs anymore. We should stop using it, and we should stop using VS Code, because it's made by Microsoft, and Microsoft made Copilot.Otherwise, we will be replaced by AI.PS: It's a joke, don't take it seriously.Don't stop using VS Code, it's the best code editor.",
    authorName: 'Maria Petrova',
    authorPhoto: 'https://picsum.photos/200',
    createdOn: {
      date: '02-08-2023',
      time: '12:55 PM',
    },
    tags: ['copilot', 'ai', 'data science'],
    category: 'MachineLearning',
    commentCount: 43,
    likeCount: 10,
    viewCount: 198,
    isYourPost: true,
    isLiked: false,
  },
  {
    id: 6,
    title: 'What do you think about Copilot',
    content:
      "It will replace us, the developers, and we won't have jobs anymore. We should stop using it, and we should stop using VS Code, because it's made by Microsoft, and Microsoft made Copilot.Otherwise, we will be replaced by AI.PS: It's a joke, don't take it seriously.Don't stop using VS Code, it's the best code editor.",
    authorName: 'Maria Petrova',
    authorPhoto: 'https://picsum.photos/200',
    createdOn: {
      date: '02-08-2023',
      time: '12:55 PM',
    },
    tags: ['copilot', 'ai', 'data science'],
    category: 'MachineLearning',
    commentCount: 43,
    likeCount: 10,
    viewCount: 198,
    isYourPost: true,
    isLiked: false,
  },
];
