import { faCode, faRobot, faMobile, faWindowMaximize, faSchool, faDatabase, faBriefcase,} from '@fortawesome/free-solid-svg-icons';

export const categories = {
  ProgrammingLanguages: faCode,
  MachineLearning: faRobot,
  MobileProgramming: faMobile,
  DataBaseAndML: faDatabase,
  WebDesign: faWindowMaximize,
  Tutorials: faSchool,
  FindYourDreamJob: faBriefcase
};
