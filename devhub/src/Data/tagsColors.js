export const tagsColors = {
  react: 'rgba(220, 0, 0,', // red
  'machine learning': 'rgb(100, 200, 0,', // green
  'react-router': 'rgba(0, 0, 180,', // blue
  python: 'rgba(255, 130, 0,', // yellow
  angular: ' rgba(25, 0, 200,', // dark blue
  javascript: 'rgba(255, 0, 200,', // magenta
  'c#': 'rgba(0, 180, 255,', // light blue
  'c++': 'rgba(0, 128, 128,', // gray
  java: 'rgba(100, 20, 180,',
  'web development': 'rgba(128, 128, 0,', // olive
  'mobile development': 'rgb(58, 158, 0,', // green
  ai: 'rgba(128, 0, 128,', // purple
  'data science': 'rgba(0, 128, 128,', // teal
  'computer science': 'rgba(0, 0, 100,', // navy
  'deep learning': 'rgba(255, 130, 0,', // orange
  'front-end': 'rgba(165, 42, 42,', // brown
  'back-end': 'rgba(200, 180, 30,', // yellow-green
  'full-stack': ' rgba(205, 100, 203,', // pink
  'data structures': 'rgba(25, 0, 255,', //dark blue
  algorithms: ' rgba(0, 255, 255,', // light blue
  copilot: 'rgba(255, 0, 98,', //purple-pink
};
