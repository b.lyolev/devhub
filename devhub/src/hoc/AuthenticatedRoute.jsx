import { useContext, useState } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import AuthContext from '../context/AuthContext';

export default function AuthenticatedRoute({ children }) {
  const { user } = useContext(AuthContext);
  let location = useLocation();

  if (user === null) {
    return <Navigate to='login' state={{ from: location.pathname }}></Navigate>;
  }

  return children;
}
