/*eslint-disable*/
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../context/AuthContext'; // Ensure you have this context

const WithAdminPrivileges = (Component) => {
  return (props) => {
    const { userData } = useContext(AuthContext);
    const navigate = useNavigate();

    if (!userData || userData.role !== 'admin') {
      navigate('/');
      return null;
    }

    return <Component {...props} />;
  };
}

export default WithAdminPrivileges;