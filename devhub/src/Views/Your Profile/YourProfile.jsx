import { Fragment, useContext, useEffect, useState } from 'react';
import { Disclosure, Menu, RadioGroup, Transition } from '@headlessui/react';
import {
  QuestionMarkCircleIcon,
  SearchIcon,
  CreditCardIcon,
  UserCircleIcon,
  ViewGridAddIcon,
  ChatAlt2Icon,
  ChatAltIcon,
  UserGroupIcon,
  BellIcon,
} from '@heroicons/react/solid';
import { CogIcon } from '@heroicons/react/outline';
import ProfileEdit from '../../components/ProfileEdit/ProfileEdit';
import UserPosts from '../../components/UserPosts/UserPosts';
import Navbar from '../../components/Navbar/NavBar';
import AuthContext from '../../context/AuthContext';
import { getUserData } from '../../services/user.service';
import AdminUsers from '../../components/AdminUsers/AdminUsers';
import UserComments from '../../components/UserComments/UserComments';
import LoadingSpinner from '../../components/Loading/Loading';

import {
  BrowserRouter,
  Link,
  Route,
  Routes,
  useNavigate,
} from 'react-router-dom';
import AllComments from '../../components/AllComments/AllComments';

const plans = [
  {
    name: 'Startup',
    priceMonthly: 29,
    priceYearly: 290,
    limit: 'Up to 5 active job postings',
  },
  {
    name: 'Business',
    priceMonthly: 99,
    priceYearly: 990,
    limit: 'Up to 25 active job postings',
  },
  {
    name: 'Enterprise',
    priceMonthly: 249,
    priceYearly: 2490,
    limit: 'Unlimited active job postings',
  },
];
const payments = [
  {
    id: 1,
    date: '1/1/2020',
    datetime: '2020-01-01',
    description: 'Business Plan - Annual Billing',
    amount: 'CA$109.00',
    href: '#',
  },
  // More payments...
];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function YourProfile() {
  const { user } = useContext(AuthContext);
  const [currentUser, setCurrentUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const navigate = useNavigate();
  useEffect(() => {
    if (user !== null) {
      const fetchUserData = async () => {
        try {
          const snapshot = await getUserData(user.uid);
          if (snapshot.exists()) {
            const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
            setCurrentUser(userData);
          } else {
            console.log("User doesn't exist");
          }
        } catch (error) {
          console.error('Error fetching user data:', error);
        } finally {
          setIsLoading(false);
        }
      };

      fetchUserData();
    }
  }, [user]);
  const [activeTab, setActiveTab] = useState('Profile');

  const isAdmin = currentUser?.role?.toLowerCase() === 'admin';

  const subNavigation = [
    {
      name: 'Profile',
      href: '#',
      icon: UserCircleIcon,
      current: true,
    },
    {
      name: 'My posts',
      href: '#',
      icon: CreditCardIcon,
      current: false,
    },
    {
      name: 'My comments',
      href: '#',
      icon: ChatAltIcon,
      current: false,
    },

    ...(isAdmin
      ? [
          {
            name: 'All users',
            href: '#',
            icon: UserGroupIcon,
            current: false,
          },

          {
            name: 'All comments',
            href: '#',
            icon: ChatAlt2Icon,
            current: false,
          },
          {
            name: 'Notifications',
            href: '#',
            icon: BellIcon,
            current: false,
          },
        ]
      : []),
  ];
  return (
    <>
      <div className='mb-20'>
        <Navbar></Navbar>
      </div>
      <div className='h-full'>
        <main className='max-w-7xl mx-auto pb-10 lg:py-12 lg:px-8'>
          <div className='lg:grid lg:grid-cols-12 lg:gap-x-5'>
            <aside className='py-6 px-2 sm:px-6 lg:py-0 lg:px-0 lg:col-span-3'>
              <nav className='space-y-1'>
                {subNavigation.map((item) => (
                  <Link
                    key={item.name}
                    to={item.href}
                    className={classNames(
                      item.name === activeTab
                        ? 'bg-gray-50 text-indigo-600 hover:bg-white'
                        : 'text-gray-900 hover:text-gray-900 hover:bg-gray-50',
                      'group rounded-md px-3 py-2 flex items-center text-sm font-medium'
                    )}
                    aria-current={item.current ? 'page' : undefined}
                    onClick={() => {
                      setActiveTab(item.name);
                      navigate(item.href);
                    }}
                  >
                    <item.icon
                      className={classNames(
                        item.name === activeTab
                          ? 'text-indigo-500'
                          : 'text-gray-400 group-hover:text-gray-500',
                        'flex-shrink-0 -ml-1 mr-3 h-6 w-6'
                      )}
                      aria-hidden='true'
                    />
                    <span className='truncate'>{item.name}</span>
                  </Link>
                ))}
              </nav>
            </aside>

            <div className='space-y-6 sm:px-6 lg:px-0 lg:col-span-9'>
              <div className='space-y-6 sm:px-6 lg:px-0 lg:col-span-9'>
                {isLoading ? (
                  <LoadingSpinner />
                ) : (
                  <>
                    {activeTab === 'Profile' &&
                      (isAdmin ? (
                        <>
                          <ProfileEdit />
                        </>
                      ) : (
                        <ProfileEdit />
                      ))}
                    {activeTab === 'My posts' && (
                      <UserPosts user={currentUser} />
                    )}
                    {activeTab === 'My comments' && (
                      <UserComments user={currentUser} />
                    )}
                    {activeTab === 'Notifications' && (
                      <UserComments user={currentUser} />
                    )}
                    {activeTab === 'All users' && (
                      <AdminUsers user={currentUser} />
                    )}
                    {activeTab === 'All comments' && (
                      <AllComments user={currentUser} />
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
}
