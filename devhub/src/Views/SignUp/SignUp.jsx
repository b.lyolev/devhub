/* eslint-disable no-unused-vars */
import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import AuthContext from "../../context/AuthContext";
import { createUserHandle, getUserByHandle } from "../../services/user.service";
import { registerUser } from "../../services/auth.service";
import { NavLink } from "react-router-dom";
import LoginModalRegistered from "../../components/LoginModal/LoginModalRegistered";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLaptopCode } from "@fortawesome/free-solid-svg-icons";

export default function SignUp() {
  const [form, setForm] = useState({
    email: "",
    password: "",
    handle: "",
  });

  const { setUser } = useContext(AuthContext);

  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const onRegister = () => {
    if (!form.email) {
      return alert("Please enter email!");
    }

    if (form.password.length < 7) {
      return alert("Password should be at least 7 characters long!");
    }

    if (form.handle.length < 4) {
      return alert("Handle should be at least 4 characters long!");
    }

    getUserByHandle(form.handle)
      .then((snapshot) => {
        if (snapshot.exists()) {
          throw new Error(`Handle @${form.handle} has already been taken!`);
        }

        return registerUser(form.email, form.password);
      })
      .then((credential) => {
        return createUserHandle(
          form.handle,
          credential.user.uid,
          credential.user.email
        ).then(() => {
          setUser({
            user: credential.user,
          });
        });
      })
      .then(() => {
        navigate("/");
      })
      .catch((e) => console.log(e));
  };

  return (
    <>
      <div className="items-center justify-center h-screen">
        <div className=" min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8 ">
          <div className="sm:mx-auto sm:w-full sm:max-w-md">
            <FontAwesomeIcon
              className="py-2 px-1 h-8 w-auto text-white max-sm:h-8"
              icon={faLaptopCode}
            />
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
              Create Account
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600">
              Already have an account? {/* <LoginModalRegistered /> */}
            </p>
          </div>

          <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div className=" py-8 px-4 md:shadow lg:shadow sm:rounded-lg sm:px-10">
              <div className="space-y-5">
                <div>
                  <label
                    htmlFor="email"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Email address
                  </label>
                  <div className="mt-1">
                    <input
                      id="email"
                      name="email"
                      type="email"
                      // autoComplete='email'
                      required
                      className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                      value={form.email}
                      onChange={updateForm("email")}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="handle"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Username
                  </label>
                  <div className="mt-1">
                    <input
                      id="handle"
                      name="handle"
                      //it will be type for username
                      type="text"
                      // autoComplete='handle'
                      required
                      className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                      value={form.handle}
                      onChange={updateForm("handle")}
                    />
                  </div>
                </div>
                <div>
                  <label
                    htmlFor="password"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Password
                  </label>
                  <div className="mt-1">
                    <input
                      id="password"
                      name="password"
                      type="password"
                      autoComplete="current-password"
                      required
                      className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                      value={form.password}
                      onChange={updateForm("password")}
                    />
                  </div>
                </div>

                <div className="flex items-center justify-between"></div>

                <div>
                  <button
                    onClick={onRegister}
                    type="submit"
                    className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Sign up
                  </button>
                </div>
              </div>

              {/* <div className='mt-6'></div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
