/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useRef, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { CheckIcon } from '@heroicons/react/outline';
import { loginUser } from '../../services/auth.service';
import { useContext } from 'react';
import AuthContext from '../../context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClose } from '@fortawesome/free-solid-svg-icons';
import { createUserHandle, getUserByHandle } from '../../services/user.service';
import { registerUser } from '../../services/auth.service';
import LoginModal from '../Login/LoginModal';
import {
  faLaptopCode,
  faMicrochip,
  faUserSecret,
} from '@fortawesome/free-solid-svg-icons';

export default function SignUpModal({ open, setOpen }) {
  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    handle: '',
  });

  const [validationMessages, setValidationMessages] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    handle: '',
  });

  const [error, setError] = useState(null);

  const { setUser } = useContext(AuthContext);

  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    const value = e.target.value;

    setForm({
      ...form,
      [prop]: value,
    });

    const newValidationMessages = { ...validationMessages };

    switch (prop) {
      case 'firstName':
        newValidationMessages.firstName =
          value.length < 4 || value.length > 32
            ? 'First name must be between 4 and 32 characters!'
            : '';
        if (!/^[A-Za-z]+$/.test(value)) {
          newValidationMessages.firstName =
            'First name can only contain letters!';
        }
        break;
      case 'lastName':
        newValidationMessages.lastName =
          value.length < 4 || value.length > 32
            ? 'Last name must be between 4 and 32 characters!'
            : '';
        if (!/^[A-Za-z\s]+$/.test(value)) {
          newValidationMessages.lastName =
            'Last name can only contain letters and spaces!';
        }
        break;
      case 'email':
        newValidationMessages.email = !value
          ? 'Please enter an email!'
          : !/^\S+@\S+\.\S+$/.test(value)
          ? 'Please enter a valid email address!'
          : '';
        break;
      case 'password':
        newValidationMessages.password =
          value.length < 7
            ? 'Password should be at least 7 characters long!'
            : '';
        if (!/[A-Za-z]/.test(value)) {
          newValidationMessages.password =
            'Password should contain at least one letter!';
        } else if (!/\d/.test(value)) {
          newValidationMessages.password =
            'Password should contain at least one digit!';
        } else if (!/[!@#$%^&.*]/.test(value)) {
          newValidationMessages.password =
            'Password should contain at least one special character!';
        }
        break;
      case 'handle':
        newValidationMessages.handle =
          value.length < 4
            ? 'Username should be at least 4 characters long!'
            : '';
        if (!/^[A-Za-z0-9_]+$/.test(value)) {
          newValidationMessages.handle =
            'Username can only contain letters, digits, and underscores!';
        }
        break;
      default:
        break;
    }

    setValidationMessages(newValidationMessages);
  };

  const onRegister = () => {
    if (
      validationMessages.firstName ||
      validationMessages.lastName ||
      validationMessages.email ||
      validationMessages.password ||
      validationMessages.handle
    ) {
      return; // Don't proceed with registration
    }
    getUserByHandle(form.handle)
      .then((snapshot) => {
        if (snapshot.exists()) {
          throw new Error(`Handle @${form.handle} has already been taken!`);
        }

        return registerUser(form.email, form.password);
      })
      .then((credential) => {
        return createUserHandle(
          form.handle,
          credential.user.uid,
          form.firstName,
          form.lastName,
          credential.user.email
        ).then(() => {
          setUser({
            user: credential.user,
          });
        });
      })
      .then(() => {
        navigate('/');
      })
      .catch((error) => setError(error.message));
  };

  const cancelButtonRef = useRef(null);

  const isFormInvalid =
    validationMessages.firstName ||
    validationMessages.lastName ||
    validationMessages.email ||
    validationMessages.password ||
    validationMessages.handle ||
    !form.firstName ||
    !form.lastName ||
    !form.email ||
    !form.password ||
    !form.handle;

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as='div'
        className='fixed z-50 inset-0 overflow-y-auto '
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <div className='flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0'>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <Dialog.Overlay className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity ' />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className='hidden sm:inline-block sm:align-middle sm:h-screen'
            aria-hidden='true'
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
            enterTo='opacity-100 translate-y-0 sm:scale-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100 translate-y-0 sm:scale-100'
            leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
          >
            <div className='relative inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6 max-sm:w-full max-sm:p-0 max-sm:m-0'>
              <div>
                <button
                  onClick={() => setOpen(false)}
                  className='absolute top-3 right-3 text-2xl rounded-md bg-indigo-100 px-2 text-gray-600 hover:text-gray-800'
                >
                  <FontAwesomeIcon icon={faClose} />
                </button>
                <div className='items-center justify-center'>
                  <div className=' min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8 '>
                    <div className='sm:mx-auto sm:w-full sm:max-w-md text-center'>
                      <FontAwesomeIcon
                        className='mx-auto h-12 w-auto align-middle'
                        icon={faLaptopCode}
                      />

                      <h2 className='mt-6 text-center text-3xl font-extrabold text-gray-900'>
                        Create Account
                      </h2>
                    </div>

                    <div className='mt-8 sm:mx-auto sm:w-full sm:max-w-md'>
                      <div className=' py-8 px-4 md:shadow lg:shadow sm:rounded-lg sm:px-10'>
                        <div className='space-y-5'>
                          <div className='flex flex-row  gap-3 max-md:flex-col'>
                            <div className='col-span-6 sm:col-span-3'>
                              <label
                                htmlFor='first-name'
                                className='block text-sm font-medium text-gray-700'
                              >
                                First name
                              </label>
                              <input
                                type='text'
                                name='first-name'
                                id='first-name'
                                autoComplete='given-name'
                                className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                                value={form.firstName}
                                onChange={updateForm('firstName')}
                              />
                              {validationMessages.firstName && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.firstName}
                                </p>
                              )}
                            </div>

                            <div className='col-span-6 sm:col-span-3'>
                              <label
                                htmlFor='last-name'
                                className='block text-sm font-medium text-gray-700'
                              >
                                Last name
                              </label>
                              <input
                                type='text'
                                name='last-name'
                                id='last-name'
                                autoComplete='family-name'
                                className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                                value={form.lastName}
                                onChange={updateForm('lastName')}
                              />
                              {validationMessages.lastName && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.lastName}
                                </p>
                              )}
                            </div>
                          </div>

                          <div>
                            <label
                              htmlFor='email'
                              className='block text-sm font-medium text-gray-700'
                            >
                              Email address
                            </label>
                            <div className='mt-1 '>
                              <input
                                id='email'
                                name='email'
                                type='email'
                                autoComplete='email'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                                value={form.email}
                                onChange={updateForm('email')}
                              />
                              {validationMessages.email && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.email}
                                </p>
                              )}
                            </div>
                          </div>

                          <div>
                            <label
                              htmlFor='handle'
                              className='block text-sm font-medium text-gray-700'
                            >
                              Username
                            </label>
                            <div className='mt-1'>
                              <input
                                id='handle'
                                name='handle'
                                type='text'
                                autoComplete='handle'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                                value={form.handle}
                                onChange={updateForm('handle')}
                              />
                              {validationMessages.handle && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.handle}
                                </p>
                              )}
                            </div>
                          </div>
                          <div>
                            <label
                              htmlFor='password'
                              className='block text-sm font-medium text-gray-700'
                            >
                              Password
                            </label>
                            <div className='mt-1'>
                              <input
                                id='password'
                                name='password'
                                type='password'
                                autoComplete='current-password'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                                value={form.password}
                                onChange={updateForm('password')}
                              />
                              {validationMessages.password && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.password}
                                </p>
                              )}
                            </div>
                          </div>

                          <div className='flex items-center justify-between'></div>

                          <div>
                            <button
                              onClick={onRegister}
                              type='submit'
                              disabled={isFormInvalid}
                              className={`w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${
                                isFormInvalid
                                  ? 'bg-indigo-300 cursor-not-allowed hover:bg-indigo-300'
                                  : 'bg-indigo-600 hover:bg-indigo-700'
                              }`}
                            >
                              Sign up
                            </button>
                          </div>
                        </div>
                        {error && (
                          <div className='text-red-500 text-sm mt-2'>
                            {error}
                          </div>
                        )}
                        {/* <div className='mt-6'></div> */}
                      </div>
                    </div>
                  </div>
                </div>

                {/* <button
                  type='button'
                  className='mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm'
                  onClick={() => setOpen(false)}
                  ref={cancelButtonRef}
                >
                  Cancel
                </button> */}
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
