import { useState, useEffect, useContext } from 'react';
import { Box, Button } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import AuthContext from '../../context/AuthContext';
import { db } from '../../config/firebase-config';
import { ref, onValue, off, set } from 'firebase/database';

import Navbar from '../../components/Navbar/NavBar';



function Settings() {
  const [users, setUsers] = useState({});
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const usersRef = ref(db, 'users');
    const unsubscribeUsers = onValue(usersRef, (snapshot) => {
      setUsers(snapshot.val());
    });

    const postsRef = ref(db, 'posts');
    const unsubscribePosts = onValue(postsRef, (snapshot) => {
      setPosts(Object.values(snapshot.val() || {}));
    });

    // Cleanup subscription on unmount
    return () => {
      off(usersRef, 'value', unsubscribeUsers);
      off(postsRef, 'value', unsubscribePosts);
    };
  }, []);

  const countUserPosts = (authorUID) => {
    return posts.filter(post => post.authorUID === authorUID).length;
  };

  const toggleCanPost = (authorUID, currentStatus) => {
    const userRef = ref(db, `users/${authorUID}/canPost`);
    set(userRef, !currentStatus);
  };

  const renderUserBox = (user, authorUID) => (
    <div key={authorUID} className="border p-6 rounded-lg shadow-md my-4 bg-white">
      <h2 className="text-xl font-semibold">{user.firstName} {user.lastName}</h2>
      <p className="mt-4">Email: {user.email}</p>
      <p className="mt-2">Posts Count: {countUserPosts(authorUID)}</p>
      <button
        className={`mt-5 px-6 py-2 rounded text-white ${user.canPost ? 'bg-red-500' : 'bg-green-500'}`}
        onClick={() => toggleCanPost(authorUID, user.canPost)}
      >
        {user.canPost ? "Block" : "Unblock"}
      </button>
    </div>
  );

  return (
    <div className="flex flex-col items-center justify-center m-5">
      <Navbar />
      <h1 className="text-2xl mb-5 font-bold">User Settings</h1>
      <div className="w-full max-w-xl">
        {Object.entries(users).map(([authorUID, user]) => renderUserBox(user, authorUID))}
        <div className="mt-4">
          <Link
            to="/admin-dashboard"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Go to Admin Dashboard
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Settings;