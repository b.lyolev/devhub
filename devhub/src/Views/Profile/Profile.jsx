import { useState, useEffect, useContext } from 'react';
import { getUserData, updateUserProfile } from '../../services/user.service';
import AuthContext from '../../context/AuthContext';

function Profile() {
  const { user } = useContext(AuthContext);
  const [userData, setUserData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUserData(user.uid).then(data => {
      setUserData(data);
      setLoading(false);
    });
  }, [user]);

  const handleUpdate = () => {
    updateUserProfile(user.uid, userData);
  };

  if (loading) return <div>Loading...</div>;

  return (
    <div className="flex flex-col items-center">

      <label> First name</label>
      <input
        className="border p-2 m-2"
        value={userData.firstName}
        onChange={e => setUserData({ ...userData, firstName: e.target.value })}
      />

      <label> Last name</label>
      <input
        className="border p-2 m-2"
        value={userData.lastName}
        onChange={e => setUserData({ ...userData, lastName: e.target.value })}
      />

      <label> Email</label>
      <input
        className="border p-2 m-2"
        value={userData.email}
        onChange={e => setUserData({ ...userData, email: e.target.value })}
      />
      <button className="bg-blue-500 text-white p-2 m-2" onClick={handleUpdate}>Update Profile</button>
    </div>
  );
}

export default Profile;