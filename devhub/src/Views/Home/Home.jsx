import Navbar from '../../components/Navbar/NavBar';
import CreatePost from '../../components/CreatePost/CreatePost';
import WelcomeSectionTest from '../../components/Header/WelcomeSection-test';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../../config/firebase-config';
import { useEffect, useState } from 'react';
import { getUserData } from '../../services/user.service';
import LoadingSpinner from '../../components/Loading/Loading';
import Categories from '../../components/Categories/Categories';
import Posts from '../../components/Posts Container/Posts';
import CategoriesMenu from '../../components/Categories/CategoriesMenu';
import Top10 from '../../components/Posts Container/Top10';
import Footer from '../../components/Footer/Footer';
import YourProfile from '../Your Profile/YourProfile';

export default function Home() {
  // const [user] = useAuthState(auth);

  const [user, loading] = useAuthState(auth);
  const [isInitialized, setIsInitialized] = useState(false);

  const [category, setCategory] = useState('');

  const handleCategoryChange = (selectedCategory) => {
    setCategory(selectedCategory);
  };

  useEffect(() => {
    if (loading) return;
    setIsInitialized(true);
  }, [loading]);

  return (
    <>
      <div className='mb-28'>
        <Navbar />
      </div>
      <div className='mx-auto max-w-7xl'>
        {loading ? (
          <div className='text-center py-4'>
            <LoadingSpinner />
          </div>
        ) : user === null ? (
          <>
            <WelcomeSectionTest />
            <div className='mt-5 mx-5 pb-4 rounded-2xl  bg-slate-50'>
              <Top10 />
            </div>
          </>
        ) : (
          <>
            {/* <YourProfile /> */}
            <CreatePost />
            <div className='mt-5 mx-5 py-4 rounded-2xl  bg-slate-50 '>
              <Categories onSelectCategory={handleCategoryChange} />
              <Posts category={category} />
            </div>
          </>
        )}

        {/* <Footer /> */}
      </div>
    </>
  );
}
