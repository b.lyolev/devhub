import React, { useEffect, useState, useContext } from 'react';
import { getUserData } from '../../services/user.service';
import { useNavigate, useParams } from 'react-router-dom';
import {
  createComment,
  deleteCommentByUID,
  deletePostAndRelatedData,
  getCommentsForPost,
  getPostData,
  getSinglePostByUID,
  incrementViewCount,
  togglePostLike,
  updateCommentContent,
  updateLikedPostsArray,
  updatePostContent,
  updatePostLikedBy,
  updatePostTitle,
} from '../../services/post.service';
import { tagsColors } from '../../Data/tagsColors';
import { Avatar } from '@chakra-ui/react';
import { css } from '@emotion/css';
import { categories } from '../../components/common/categories';
import AuthContext from '../../context/AuthContext';
import Navbar from '../../components/Navbar/NavBar';
import { Menu } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import ReactMarkdown from 'react-markdown';
import LoadingSpinner from '../../components/Loading/Loading';
import {
  faFloppyDisk,
  faPenToSquare,
  faTrashCan,
  faEye,
  faHeart,
  faComment,
  faTimes,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  formatDate,
  formatRelativeTime,
  formatTime,
} from '../../components/common/formatDateTime';
import Alert from '../../components/Alert/Alert';

export default function PostPreview() {
  const sortOptions = [{ name: 'Most Recent' }, { name: 'Oldest' }];
  const { user, userData, setUser } = useContext(AuthContext);

  function classNames(...classes) {
    return classes.filter(Boolean).join(' ');
  }
  const [currentUser, setCurrentUser] = useState(null);

  const [currentUserLoading, setCurrentUserLoading] = useState(true);

  const { postId } = useParams();

  const [post, setPost] = useState(null);

  // console.log(postId);

  const [commentContent, setCommentContent] = useState('');

  const [editedCommentContent, setEditedCommentContent] = useState({});

  const [isEditing, setIsEditing] = useState(false);
  const [titleError, setTitleError] = useState('');

  const [isEditingTitle, setIsEditingTitle] = useState(false);
  const [isEditingContent, setIsEditingContent] = useState(false);

  const [editedTitle, setEditedTitle] = useState('');
  const [editedContent, setEditedContent] = useState('');

  // setEditedTitle(post?.title);
  // setEditedContent(post?.content);

  const handleSaveTitle = async () => {
    if (editedTitle.length < 16 || editedTitle.length > 64) {
      setTitleError('Title must be between 16 and 64 characters.');
      return;
    }
    try {
      await updatePostTitle(postId, editedTitle);
      setPost((prevPost) => ({
        ...prevPost,
        title: editedTitle,
      }));
      setIsEditingTitle(false); // Turn off editing mode
    } catch (error) {
      console.error('Error updating post title:', error);
    }
  };

  const [openDeleteAlert, setOpenDeleteAlert] = useState(false);

  const navigate = useNavigate();

  const handleDeleteAlertOpen = () => {
    setOpenDeleteAlert(true);
  };

  const handleSaveContent = async () => {
    try {
      await updatePostContent(postId, editedContent);
      setPost((prevPost) => ({
        ...prevPost,
        content: editedContent,
      }));
      setIsEditingContent(false); // Turn off editing mode
    } catch (error) {
      console.error('Error updating post content:', error);
    }
  };

  const [selectedSort, setSelectedSort] = useState(sortOptions[0].name);

  const handleEditContentChange = (e, comment) => {
    setEditedCommentContent((prevContent) => ({
      ...prevContent,
      [comment.uid]: e.target.value,
    }));
  };

  const handleCancelEdit = (comment) => {
    setEditedCommentContent((prevContent) => ({
      ...prevContent,
      [comment.uid]: comment.content,
    }));

    setEditingCommentId(null);
  };

  const handleSaveEdit = async (comment) => {
    try {
      const editedContent = editedCommentContent[comment.uid];
      if (!editedContent || editedContent.trim() === '') {
        console.log('Edited content is empty or null');
        return;
      }

      await updateCommentContent(comment.uid, editedContent);

      setComments((prevComments) =>
        prevComments.map((c) =>
          c.uid === comment.uid ? { ...c, content: editedContent } : c
        )
      );

      setEditedCommentContent((prevContent) => ({
        ...prevContent,
        [comment.uid]: undefined,
      }));

      // Clear the editing state
      setEditingCommentId(null);
    } catch (error) {
      console.error('Error saving edited comment:', error);
    }
  };
  const handleSortSelect = (sortName) => {
    setSelectedSort(sortName);
  };

  const handleSaveClick = async () => {
    if (editedTitle.length < 16 || editedTitle.length > 64) {
      setTitleError('Title must be between 16 and 64 characters.');
      return;
    }
    try {
      await updatePostTitle(postId, editedTitle);

      setPost((prevPost) => ({
        ...prevPost,
        title: editedTitle,
      }));

      setIsEditing(false);
    } catch (error) {
      console.error('Error updating post title:', error);
    }
  };

  const [liked, setLiked] = useState(false);

  const handleLikeClick = async () => {
    try {
      const isLiked = await togglePostLike(
        user.uid,
        postId,
        currentUser.handle,
        liked
      );
      setLiked(isLiked);

      setPost((prevPost) => {
        const updatedLikedBy = { ...prevPost.likedBy };
        if (isLiked) {
          updatedLikedBy[user.uid] = true;
        } else {
          delete updatedLikedBy[user.uid];
        }

        return {
          ...prevPost,
          likedBy: updatedLikedBy,
        };
      });
    } catch (error) {
      console.error('Error handling like:', error);
    }
  };

  const [comments, setComments] = useState([]);
  const [editingCommentId, setEditingCommentId] = useState(null);

  useEffect(() => {
    const fetchComments = async () => {
      try {
        getCommentsForPost(postId, (commentsData) => {
          if (commentsData.length > 0) {
            const updatedComments = commentsData.map(async (comment) => {
              try {
                const snapshot = await getUserData(comment.authorUID);
                if (snapshot.exists()) {
                  const authorInfo =
                    snapshot.val()[Object.keys(snapshot.val())[0]];
                  comment.authorInfo = authorInfo;
                  comment.isCurrentUserComment = comment.authorUID === user.uid;
                }
              } catch (error) {
                console.error('Error fetching authorInfo:', error);
              }
              return comment;
            });

            Promise.all(updatedComments).then((resolvedComments) => {
              setComments(resolvedComments);
            });
          }
        });
      } catch (error) {
        console.error('Error fetching comments:', error);
      }
    };

    fetchComments();
  }, [postId]);

  // console.log(post);
  const handleDeleteClick = async () => {
    if (user === null) return;
    try {
      const isDeleted = await deletePostAndRelatedData(post.uid);

      if (isDeleted) {
        navigate('/');
      } else {
        console.log('Post not found.');
      }
    } catch (error) {
      console.error('Error deleting post:', error);
    }
  };
  // console.log(comments);

  const [viewCounted, setViewCounted] = useState(false);
  useEffect(() => {
    const fetchPostData = async () => {
      try {
        const snapshot = await getSinglePostByUID(postId);
        if (snapshot.exists()) {
          const postData = snapshot.val();
          setPost(postData);

          if (!viewCounted) {
            try {
              // Call the function to increment the view count
              await incrementViewCount(postId);

              const isLikedByCurrentUser =
                postData.likedBy && postData.likedBy[user.uid]; // Use postData here

              setLiked(isLikedByCurrentUser);
              // Mark the view count as updated to prevent further increments
              setViewCounted(true);
            } catch (error) {
              console.error('Error incrementing view count:', error);
            }
          }
        } else {
          console.log("Post doesn't exist");
        }
      } catch (error) {
        console.error('Error fetching post data:', error);
      }
    };

    fetchPostData();
  }, [postId, viewCounted]);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const snapshot = await getUserData(user.uid);
        if (snapshot.exists()) {
          const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
          setCurrentUser(userData);
        } else {
          console.log("User doesn't exist");
        }
      } catch (error) {
        console.error('Error fetching user data:', error);
      } finally {
        setCurrentUserLoading(false);
      }
    };

    fetchUserData();
  }, [user.uid]);

  if (!post) {
    return <LoadingSpinner />;
  }

  const isYourPost = post.authorUID === user.uid;

  const selectedCategory = categories.find(
    (currentCategory) => currentCategory.name === post.category
  );

  const handleAddComment = async () => {
    if (commentContent.trim() === '') {
      alert('Please enter a comment.');

      return;
    }

    try {
      await createComment(
        user.uid,
        commentContent,
        post.uid,
        currentUser.handle
      );

      setCommentContent('');
    } catch (error) {
      console.error('Error creating comment:', error);
    }
  };

  const handleDeleteComment = async (comment) => {
    try {
      console.log(comment.uid, comment.authorUID, post.uid, currentUser.handle);
      await deleteCommentByUID(
        comment.uid,
        comment.authorUID,
        comment.postUID,
        comment.authorHandle
      );
      setComments((prevComments) =>
        prevComments.filter((c) => c.uid !== comment.uid)
      );
    } catch (error) {
      console.error('Error deleting comment:', error);
    }
  };

  return (
    <>
      <Navbar />
      <div className='mt-28 pb-10 px-5'>
        <div>
          {currentUserLoading ? (
            <LoadingSpinner />
          ) : (
            <>
              <div className='mx-auto max-w-7xl pb-4  rounded-2xl shadow-lg  bg-slate-50'>
                <div className='category&creator flex justify-between   text-sm text-white mb-5'>
                  <div className='category font-normal bg-slate-800 p-2 rounded-br-xl rounded-tl-xl'>
                    <a className='font-medium text-indigo-400 '>
                      <FontAwesomeIcon
                        className=' mr-1'
                        icon={selectedCategory.icon}
                      />{' '}
                      {post.category}
                    </a>
                  </div>
                  <div className='creator'>
                    <div className='authorName font-normal bg-slate-800 p-2 rounded-bl-xl rounded-tr-xl'>
                      created on:{' '}
                      <a className='font-medium text-indigo-300'>
                        {formatDate(post.createdOn.dateTime)}{' '}
                        {formatTime(post.createdOn.dateTime)}{' '}
                      </a>
                      by{' '}
                      <a className='font-medium text-indigo-300'>
                        {post.author}
                      </a>
                    </div>
                  </div>
                </div>

                <div className='text-center text-3xl font-bold mt-3 mb-6 mx-32'>
                  {isEditingTitle ? (
                    <>
                      <div className='flex flex-row'>
                        <input
                          type='text'
                          value={editedTitle}
                          rows={Math.max(1, Math.ceil(post.title.length / 50))}
                          onChange={(e) => setEditedTitle(e.target.value)}
                          className='flex-1 w-full border-0 rounded-lg bg-slate-200 text-center resize-none ring-2 focus:ring-2 sm:text-base'
                        />

                        <div className='flex flex-col ml-2'>
                          {' '}
                          <button
                            className='   text-base'
                            onClick={handleSaveTitle}
                          >
                            <FontAwesomeIcon icon={faFloppyDisk} />
                          </button>
                          <button
                            className='  text-base'
                            onClick={() => {
                              setIsEditingTitle(false);
                              setEditedTitle(post.title);
                            }}
                          >
                            <FontAwesomeIcon icon={faTimes} />{' '}
                          </button>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      {post.title}
                      {isYourPost && (
                        <button
                          className=' text-base ml-2 p-1 align-top'
                          onClick={() => {
                            setIsEditingTitle(true);
                            setEditedTitle(post.title);
                          }}
                        >
                          <FontAwesomeIcon icon={faPenToSquare} />
                        </button>
                      )}
                    </>
                  )}
                </div>
                <div className='postContent mx-10  '>
                  <div className='postContentText flex flex-col gap-3 text-lg p-7  ring-2 font-medium  rounded-3xl shadow-lg  '>
                    {isEditingContent ? (
                      <>
                        <textarea
                          rows={Math.max(
                            1,
                            Math.ceil(post.content.length / 50)
                          )}
                          value={editedContent}
                          onChange={(e) => setEditedContent(e.target.value)}
                          className='flex-1 w-full border-0 rounded-lg bg-slate-200 resize-none ring-2 focus:ring-2 sm:text-base'
                        />
                        <div className='flex flex-row justify-end'>
                          <button
                            className='flex m-1 p-1'
                            onClick={handleSaveContent}
                          >
                            <FontAwesomeIcon icon={faFloppyDisk} />
                          </button>
                          <button
                            className='flex  m-1 p-1'
                            onClick={() => {
                              setIsEditingContent(false);
                              setEditedContent(post.content);
                            }}
                          >
                            <FontAwesomeIcon icon={faTimes} />
                          </button>
                        </div>
                      </>
                    ) : (
                      <>
                        <ReactMarkdown>{post.content}</ReactMarkdown>

                        {isYourPost && (
                          <button
                            className='flex flex-1 justify-end m-1 p-1'
                            onClick={() => {
                              setIsEditingContent(true);
                              setEditedContent(post.content);
                            }}
                          >
                            <FontAwesomeIcon icon={faPenToSquare} />
                          </button>
                        )}
                      </>
                    )}
                  </div>
                  <div className='postContentBottom flex justify-between mt-8 mb-3 px-5 items-center'>
                    <div className='tags flex flex-row gap-2'>
                      {post.tags.length > 0 ? (
                        <div className='tags flex flex-row gap-2'>
                          {post.tags.split(',').map((tag, index) => (
                            <span
                              key={index}
                              className={css`
            background-color: ${tagsColors[tag]}0.7);
            border: 1px solid ${tagsColors[tag]}0.8);
            display: block;
            padding: 2px 6px;
            font-size: 1rem;
            line-height: 18px;
            color: #fff;
            border-radius: 4px;
            cursor: pointer;
            transition: 'all';         
            transition-duration: 200;
            &:hover {
              background-color: ${tagsColors[tag]}0.4);
              color: black;
              border: 1px solid ${tagsColors[tag]}0.8);
              transition-duration: 200;
            }
          `}
                            >
                              {tag}
                            </span>
                          ))}
                        </div>
                      ) : (
                        <div className='text-lg font-medium text-slate-800'>
                          No tags available.
                        </div>
                      )}
                    </div>
                    <div>
                      <div className='flex gap-2 text-lg font-bold mr-1 '>
                        <div className='gap-1 flex  items-center px-2.5 py-0.5 rounded-md text-base  bg-slate-200 max-md:text-md hover:bg-purple-300 duration-300 cursor-pointer max-md:text-lg max-sm:text-sm '>
                          <span>{post.views || 0}</span>
                          <FontAwesomeIcon icon={faEye} />
                        </div>
                        <div
                          className='gap-1 flex items-center px-2.5 py-0.5 rounded-md text-base font-bold bg-slate-200 max-md:text-md hover:bg-purple-300 duration-300 cursor-pointer max-md:text-lg max-sm:text-sm '
                          onClick={handleLikeClick}
                        >
                          <span>
                            {post.likedBy
                              ? Object.keys(post.likedBy).length
                              : 0}
                          </span>
                          <FontAwesomeIcon
                            className={`text-${
                              liked ? 'red-500' : 'gray-500'
                            } hover:scale-125 duration-300`}
                            icon={faHeart}
                          />
                        </div>
                        <div className='gap-1 flex  items-center px-2.5  py-0.5 rounded-md text-base font-bold bg-slate-200 max-md:text-md hover:bg-purple-300 duration-300 cursor-pointer max-md:text-lg max-sm:text-sm '>
                          <span>
                            {post.comments
                              ? Object.keys(post.comments).length
                              : 0}
                          </span>
                          <div>
                            <FontAwesomeIcon icon={faComment} />
                          </div>
                        </div>
                        {isYourPost ||
                        currentUser.role?.toLowerCase() === 'admin' ? (
                          <>
                            <button
                              onClick={handleDeleteAlertOpen}
                              className='ml-2 flex items-center gap-1 rounded-lg bg-gray-800 shadow-md px-2.5 py-2.5 m-auto text-sm font-semibold text-gray-100 cursor-pointer hover:bg-indigo-600 duration-300 max-md:text-md max-sm:text-sm max-md:p-1.5'
                            >
                              <FontAwesomeIcon icon={faTrashCan} />
                            </button>
                            {console.log('yes')}
                            <Alert
                              open={openDeleteAlert}
                              setToFalse={() => setOpenDeleteAlert(false)}
                              handleConfirmDelete={handleDeleteClick}
                            />
                          </>
                        ) : (
                          <></>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {currentUser && currentUser.isBlocked && (
                <p className='absolute h-32  px-24 right-0 left-0 bottom-[11rem] items-center justify-center  z-10 max-sm:text-base text-lg text-red-500 font-bold text-center '>
                  <FontAwesomeIcon
                    className='mr-4 text-3xl max-sm:text-xl '
                    icon={faExclamationTriangle}
                  />
                  You are blocked from commenting on posts.
                </p>
              )}

              <div
                className={`comment mx-auto max-w-7xl mt-3   rounded-2xl ${
                  currentUser && currentUser.isBlocked
                    ? 'filter blur-2 opacity-70 pointer-events-none transition duration-300 select-none'
                    : ''
                }`}
              >
                <div className='flex items-start space-x-4 p-5'>
                  <div className='flex-shrink-0 '>
                    {currentUser ? (
                      <Avatar
                        size='md'
                        className='shadow-lg shadow-indigo-200'
                        name={
                          currentUser?.firstName && currentUser?.lastName
                            ? currentUser.firstName + ' ' + currentUser.lastName
                            : ''
                        }
                        src={currentUser?.profileImage}
                      />
                    ) : (
                      <LoadingSpinner />
                    )}
                  </div>
                  <div className='min-w-0 flex-1 bg-white rounded-lg    shadow-lg shadow-indigo-200'>
                    <div className='textWithButton   '>
                      <div className='flex flex-row  rounded-lg shadow-sm overflow-hidden  focus-within:border-indigo-500 focus-within:ring-2 focus-within:ring-indigo-500'>
                        <label htmlFor='comment' className='sr-only'>
                          Add your comment
                        </label>
                        <textarea
                          rows={3}
                          name='comment'
                          id='comment'
                          className='block w-full my-1  border-0 resize-none focus:ring-0 sm:text-sm'
                          placeholder='Add your comment...'
                          value={commentContent}
                          onChange={(e) => setCommentContent(e.target.value)}
                          disabled={currentUser && currentUser.isBlocked}
                        />
                        <button
                          type='submit'
                          onClick={handleAddComment}
                          className='flex items-center m-3 px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 max-sm:m-1 max-sm:px-2 max-sm:py-1 max-sm:text-xs'
                        >
                          Add comment
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='Sorting mx-auto max-w-7xl mt-3 px-10 py-10  rounded-2xl shadow-xl bg-white'>
                <div className='flex items-center justify-between mx-5 mb-3'>
                  <div className='text-xl font-bold'>
                    {selectedSort} Comments
                  </div>
                  <Menu
                    as='div'
                    className='relative z-10 inline-block text-left'
                  >
                    <div>
                      <Menu.Button className='group inline-flex justify-center text-lg font-medium text-gray-700 hover:text-gray-900'>
                        Sort
                        <ChevronDownIcon
                          className='flex-shrink-0 -mr-1 ml-1 h-8 w-8 text-gray-400 group-hover:text-gray-500'
                          aria-hidden='true'
                        />
                      </Menu.Button>
                    </div>

                    <Menu.Items className='origin-top-left absolute right-0 z-10 mt-2 w-40 rounded-md shadow-2xl bg-white ring-1 ring-black ring-opacity-5 focus:outline-none'>
                      <div className='py-1'>
                        {sortOptions.map((option) => (
                          <Menu.Item key={option.name}>
                            {({ active }) => (
                              <a
                                href={option.href}
                                onClick={() => handleSortSelect(option.name)} // Call the handler
                                className={classNames(
                                  active ? 'bg-gray-100' : '',
                                  'block px-4 py-2 text-sm font-medium text-gray-900'
                                )}
                              >
                                {option.name}
                              </a>
                            )}
                          </Menu.Item>
                        ))}
                      </div>
                    </Menu.Items>
                  </Menu>
                </div>
                <div className='comment section  flex flex-col gap-5 '>
                  <hr className='border-indigo-500 ' />
                  {comments.length === 0 && !viewCounted && (
                    <div className='text-center text-lg font-bold'>
                      No comments yet
                    </div>
                  )}
                  {comments.length > 0 ? (
                    comments
                      .sort((a, b) => {
                        if (selectedSort === 'Most Recent') {
                          return b.createdOn.timestamp - a.createdOn.timestamp;
                        } else if (selectedSort === 'Oldest') {
                          return a.createdOn.timestamp - b.createdOn.timestamp;
                        }
                        return 0;
                      })
                      .map((comment) => (
                        <div
                          key={comment.uid}
                          className={`comment flex  items-center bg-slate-100 p-3 rounded-xl shadow-md ${
                            comment.isCurrentUserComment ||
                            currentUser.role?.toLowerCase() === 'admin'
                              ? 'relative'
                              : ''
                          }`}
                        >
                          <div className='absolute top-0 right-0 m-1 p-1 space-x-2 '>
                            {currentUser.role?.toLowerCase() === 'admin' &&
                              !comment.isCurrentUserComment && (
                                <button
                                  onClick={() => handleDeleteComment(comment)}
                                >
                                  <FontAwesomeIcon icon={faTrashCan} />
                                </button>
                              )}
                          </div>
                          {comment.isCurrentUserComment && (
                            <div className='absolute top-0 right-0 m-1 p-1 space-x-2'>
                              {editingCommentId === comment.uid ? (
                                <>
                                  <button
                                    onClick={() => handleSaveEdit(comment)}
                                  >
                                    {' '}
                                    <FontAwesomeIcon icon={faFloppyDisk} />
                                  </button>

                                  <button
                                    className='absolute top-5 right-0 m-1 p-1'
                                    onClick={() => handleCancelEdit(comment)}
                                  >
                                    <FontAwesomeIcon icon={faTimes} />{' '}
                                  </button>
                                </>
                              ) : (
                                <>
                                  <button
                                    onClick={() => {
                                      setEditingCommentId(comment.uid);
                                    }}
                                  >
                                    <FontAwesomeIcon icon={faPenToSquare} />
                                  </button>
                                  <button
                                    onClick={() => handleDeleteComment(comment)}
                                  >
                                    <FontAwesomeIcon icon={faTrashCan} />
                                  </button>
                                </>
                              )}
                            </div>
                          )}

                          <Avatar
                            size='md'
                            name={
                              comment.authorInfo.firstName +
                              ' ' +
                              comment.authorInfo.lastName
                            }
                            src={comment.authorInfo.profileImage}
                          />

                          <div className='ml-3 flex-1 pr-5'>
                            {editingCommentId === comment.uid ? ( // Step 3: Conditionally render input field in edit mode
                              <textarea
                                className='flex-1 w-full border-0 rounded-lg bg-slate-200 resize-none ring-2 focus:ring-2 sm:text-base'
                                rows={Math.max(
                                  1,
                                  Math.ceil(comment.content.length / 50)
                                )}
                                value={
                                  editedCommentContent[comment.uid] ||
                                  comment.content
                                }
                                onChange={(e) =>
                                  handleEditContentChange(e, comment)
                                }
                              />
                            ) : (
                              <>
                                <p className='text-sm font-semibold text-slate-600 '>
                                  <a className='font-bold text-slate-800 mb-1'>
                                    {comment.authorInfo.firstName +
                                      ' ' +
                                      comment.authorInfo.lastName}
                                  </a>{' '}
                                  {comment.edited ? (
                                    <>
                                      edited{' '}
                                      {formatRelativeTime(
                                        comment.edited.timestamp
                                      )}
                                    </>
                                  ) : (
                                    <>
                                      commented{' '}
                                      {formatRelativeTime(
                                        comment.createdOn.timestamp
                                      )}
                                    </>
                                  )}
                                </p>
                                <p className='text-lg font-medium'>
                                  {editedCommentContent[comment.uid] ||
                                    comment.content}
                                </p>
                              </>
                            )}
                          </div>
                        </div>
                      ))
                  ) : viewCounted ? (
                    <div className='text-center text-lg font-bold'>
                      No comments yet - be the first to comment!
                    </div>
                  ) : (
                    <LoadingSpinner />
                  )}
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
}
