import { useState, useEffect } from 'react';
import { getAllUsersPosts } from '../../services/user.service';
import Navbar from '../../components/Navbar/NavBar';

import { db } from '../../config/firebase-config';
import { ref, onValue, off } from 'firebase/database';

const AdminDashboard = () => {
  const [usersData, setUsersData] = useState({});
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    async function fetchUsersPosts() {
      // ... (presuming your getAllUsersPosts logic is here or it's fetched from somewhere else)
      const data = await getAllUsersPosts();
      setUsersData(data);
    }

    const postsRef = ref(db, 'posts');
    const unsubscribePosts = onValue(postsRef, (snapshot) => {
      setPosts(Object.values(snapshot.val() || {}));
    });

    fetchUsersPosts();

    return () => {
      off(postsRef, 'value', unsubscribePosts);
    };
  }, []);

  const countUserPosts = (authorUID) => {
    return posts.filter(post => post.authorUID === authorUID).length;
  };

  return (
    <div className="flex flex-col min-h-screen">
      <Navbar />
      <div className="flex-1 p-6 mt-4">
        <h2 className="text-2xl font-bold mb-4">Admin Dashboard</h2>
        <table className="min-w-full bg-white shadow-md rounded-lg overflow-hidden">
          <thead className="bg-gray-100">
            <tr>
              <th className="py-2 px-4 text-left">Name</th>
              <th className="py-2 px-4 text-left">Email</th>
              <th className="py-2 px-4 text-left">Posts Count</th>
            </tr>
          </thead>
          <tbody>
            {Object.entries(usersData).map(([authorUID, user]) => (
              <tr key={authorUID} className="hover:bg-gray-50 transition duration-150 ease-in-out">
                <td className="border-t py-2 px-4">{user.firstName} {user.lastName}</td>
                <td className="border-t py-2 px-4">{user.email}</td>
                <td className="border-t py-2 px-4">{countUserPosts(authorUID)}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default AdminDashboard;