/** @type {import('tailwindcss').Config} */
// const withMT = require('@material-tailwind/react/utils/withMT');
export default {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx}',
    './src/components/**/*.{js,ts,jsx}',
    // 'path-to-your-node_modules/@material-tailwind/react/components/**/*.{js,ts,jsx,tsx}',
    // 'path-to-your-node_modules/@material-tailwind/react/theme/components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      blur: {
        2: '2px', // Customize the blur intensity if needed
      },
      backgroundImage: {
        'gradient-balls': "url('./src/images/3.jpg')",
        'abstract-bg': "url('./src/images/2.jpg')",
        logo: "url('./src/images/logo.png')",
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
