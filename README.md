# DevHub - A Forum for Programmers

Welcome to DevHub Forum, a platform where programmers and developers can connect, share insights, and collaborate on technical topics.

## Technologies Used

- React
- Firebase (Authentication and Realtime Database)
- Tailwind CSS
- Chakra UI

## Hosting

The project is hosted on [Firebase Hosting](https://firebase.google.com/products/hosting).

**Live Demo**: [DevHub](https://forum-testing-995f7.web.app/)

## Project Description

DevForum is a specialized forum catering to the needs of programmers and developers. Users can register, log in, create posts, add comments, and like/unlike content. Admins have additional privileges to manage users and content.

## Screenshots

Here's an overview of how we conceptualized the project using Excalidraw:

### Anonymous user

![Excalidraw Diagram](/readme-imgs/excalidraw-anonymous-view.png)

### Logged User

![Excalidraw Diagram](/readme-imgs/excalidraw-logged-view.png)

### Mobile view

![Excalidraw Diagram](/readme-imgs/excalidraw-mobile-view.png)

## Getting Started

To run the project locally, follow these steps:

1. Clone this repository: `git clone https://gitlab.com/devhub8095331/devhub.git`
2. Navigate to the project directory: `cd devhub`
3. Install dependencies: `npm install`
4. Run the development server: `npm run dev`

## Features

- User Authentication (handled by Firebase)
- User Registration and Login
- Browse Posts with Sorting and Filtering Options
- View and Interact with Posts and Comments
- User Profile Management (including profile photo upload)
- Create, Edit, and Delete Posts and Comments
- Admin Privileges (User Management, Post Deletion)

## Usage

- As an anonymous user, you can browse technical posts, sort and filter them, and view post details.
- After registering and logging in, you can create technical posts, comment on posts, and interact with other developers.
- Admins have the ability to manage users, delete posts, and maintain forum quality.

## Project Overview

### Anonymous User

![Excalidraw Diagram](/readme-imgs/home-view-anonymous.png)

### Home view for logged user

![Excalidraw Diagram](/readme-imgs/home-view-logged.png)

### Post preview

![Excalidraw Diagram](/readme-imgs/post-view.png)

### Edit profile

![Excalidraw Diagram](/readme-imgs/profile-edit.png)

### Admin dashboard

![Excalidraw Diagram](/readme-imgs/admin-dashboard.png)

## Contributions

This project was developed by Bozhidar, Velislav, Denis. Contributions are evident in the Git commit history.

## License

This project is licensed under the [MIT License](LICENSE).
